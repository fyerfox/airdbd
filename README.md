# Airdbd

The air noise database.


### Group Members:
```
   Vivian Cox
        EID:        stc783
        GitLab ID:  f1urps
        Estimated Completion Time:  30 hours
        Actual Completion Time:     40 hours
    Silvana Borgo
        EID:        sb53564
        GitLab ID:  sborgo
        Estimated Completion Time:  25 hours
        Actual Completion Time:     29 hours
    Annan Wang
        EID:        aw34928
        GitLab ID:  Nirvana_W
        Estimated Completion Time:  25 hours
        Actual Completion Time:     23 hours
    Danny Tran
        EID:        dmt2593
        GitLab ID:  fyerfox
        Estimated Completion Time:  35 hours
        Actual Completion Time:     43 hours
```


### Git SHA:
    c0f7c1b6be38252cfadd8823a7617934f15028af

### Project Leader:
    Danny Tran

### GitLab Pipelines:
[https://gitlab.com/fyerfox/airdbd/pipelines](https://gitlab.com/fyerfox/airdbd/pipelines)

### Website Link:
[https://www.airdbd.net](https://www.airdbd.net)

### Postman API Docs Link:
[https://documenter.getpostman.com/view/4253534/SW7XZp6Q?version=latest](https://documenter.getpostman.com/view/4253534/SW7XZp6Q?version=latest)
    
### UML Diagram
![](UML_airdbd.png)

### Comments:
    (None)
