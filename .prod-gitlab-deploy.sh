#!/usr/bin/env bash
# Install AWS CLI
export LC_ALL=C.UTF-8
pip install awsebcli --upgrade --ignore-installed
# Prepare deployment
mkdir ~/.aws/
rm ~/.aws/credentials
touch ~/.aws/credentials
printf "[eb-cli]\naws_access_key_id = %s\naws_secret_access_key = %s\n" "$AWS_ACCESS_KEY_ID" "$AWS_SECRET_ACCESS_KEY" >> ~/.aws/credentials
rm ~/.aws/config
touch ~/.aws/config
printf "[profile eb-cli]\nregion=ap-southeast-1\noutput=json" >> ~/.aws/config
