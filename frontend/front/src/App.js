import React, { Component } from "react";
import "./App.css";
import * as d3 from "d3";
import {
  About,
  Home,
  AirplaneModel,
  AirportModel,
  CityModel,
  FlightModel,
  Navbar,
  Footer,
  CityInstance,
  FlightInstance,
  AirportInstance,
  AirplaneInstance,
  AirplaneTypeModel,
  AirplaneTypeInstance,
  Search,
  VisualizationPage
} from "./components";
import {
  Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import ScrollToTop from "./ScrollToTop";
import { createBrowserHistory } from "history";

class App extends Component {
  render() {

    const history = createBrowserHistory();
    return (
      <Router history={history}>
        <ScrollToTop>
          <div className="App">
            {/* Navigation Bar */}
            <Navbar history={history}/>

            <div id="page">
              {/* Page Component goes here.
                Replace these contents with an
                instance of homePage, aboutPage,
                modelPage, or instancePage, depending
                on what page the user is currently
                viewing. */}
              <Switch>
                <Route exact path="/about" component={About} />
                <Route exact path="/search/:search" component={Search} />
                <Route exact path="/cities" component={CityModel} />
                <Route exact path="/flights" component={FlightModel} />
                <Route exact path="/airports" component={AirportModel} />
                <Route exact path="/airplanes" component={AirplaneModel} />
                <Route exact path="/visualize" component={VisualizationPage} />
                <Route
                  exact
                  path="/airplanetypes"
                  component={AirplaneTypeModel}
                />
                <Route path="/city/:instanceId" component={CityInstance} />
                <Route path="/flight/:instanceId" component={FlightInstance} />
                <Route
                  path="/airport/:instanceId"
                  component={AirportInstance}
                />
                <Route
                  path="/airplane/:instanceId"
                  component={AirplaneInstance}
                />
                <Route
                  path="/airplanetype/:instanceId"
                  component={AirplaneTypeInstance}
                />
                <Route exact path="/index.html">
                  <Redirect to="/" />
                </Route>
                <Route exact path="/" component={Home} />
              </Switch>
            </div>

            {/* Footer */}
            <Footer />
          </div>
        </ScrollToTop>
      </Router>
    );
  }
}

export default App;
