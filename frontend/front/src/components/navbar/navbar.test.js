import React from "react";
import { expect } from "chai";
import { shallow, mount, render } from "enzyme";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Navbar } from "../";

Enzyme.configure({ adapter: new Adapter() });

describe("<Navbar />", () => {
  // it("renders header correctly", () => {
  //   const wrapper = shallow(<Navbar />);
  //   expect(
  //     wrapper.containsMatchingElement(
  //       <div></div>
  //     )
  //   ).to.equal(true);
  // });

  it("renders items correctly", () => {
    const wrapper = shallow(<Navbar />);
    expect(wrapper.find("li").length).to.equal(8);
  });

  // it("renders text of items correctly", () => {
  //   const wrapper = shallow(<Navbar />);
  //   expect(wrapper.containsMatchingElement(<a className="nav-link">CITIES</a>)).to.equal(true);
  //   expect(wrapper.containsMatchingElement(<a className="nav-link">FLIGHTS</a>)).to.equal(
  //     true
  //   );
  //   expect(wrapper.containsMatchingElement(<a className="nav-link">AIRPORTS</a>)).to.equal(
  //     true
  //   );
  //   expect(wrapper.containsMatchingElement(<a className="nav-link">PLANE TYPES</a>)).to.equal(
  //     true
  //   );
  //   expect(wrapper.containsMatchingElement(<a className="nav-link">AIRPLANES</a>)).to.equal(
  //     true
  //   );
  //   expect(wrapper.containsMatchingElement(<a className="nav-link">ABOUT</a>)).to.equal(true);
  // });
});
