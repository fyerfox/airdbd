import React, { Component } from "react";
import "./navbar.css";
import { LinkContainer } from "react-router-bootstrap";
import SearchBar from "../search/searchBar"
import Logo from "../../pics/logo.png";

// Navigation bar at the top of every page.
// Has a logo and links to model pages and about page.
// Child of App component.

class AirdBDNavbar extends Component {
  render() {
    return (
        <nav className="navbar navbar-expand-md navbar-default fixed-top shadow-lg p-3 mb-5 rounded">
          <div className="container">
            <LinkContainer to="/">
              <div className="navbar-header">
                <a className="navbar-brand" href="/">
                  <img src={Logo} className="logo"/>
                  AirdBD
                </a>
              </div>
            </LinkContainer>

            <div id="myNavbar">
              <ul className="nav navbar-nav navbar-left">

                <li className="nav-item">
                  <LinkContainer to="/cities">
                    <a className="nav-link rounded">CITIES</a>
                  </LinkContainer>
                </li>

                <li className="nav-item">
                  <LinkContainer to="/flights">
                    <a className="nav-link rounded">FLIGHTS</a>
                  </LinkContainer>
                </li>

                <li className="nav-item">
                  <LinkContainer to="/airports">
                    <a className="nav-link rounded">AIRPORTS</a>
                  </LinkContainer>
                </li>

                <li className="nav-item">
                  <LinkContainer to="/airplanetypes">
                    <a className="nav-link rounded">PLANE TYPES</a>
                  </LinkContainer>
                </li>

                <li className="nav-item">
                  <LinkContainer to="/airplanes">
                    <a className="nav-link rounded">AIRPLANES</a>
                  </LinkContainer>
                </li>

               <li className="nav-item">
                  <LinkContainer to="/visualize">
                    <a className="nav-link rounded">VISUALIZE</a>
                  </LinkContainer>
               </li> 

               <li className="nav-item">
                  <LinkContainer to="/about">
                    <a className="nav-link rounded">ABOUT</a>
                  </LinkContainer>
                </li>
                
                <li className="nav-item">
                  <SearchBar onSearch={ (search)=>{this.props.history.push("/search/" + search)}} refresh={true}/>
                </li>
              </ul>
            </div>
          </div>
        </nav>
    );
  }
}

export default AirdBDNavbar;
