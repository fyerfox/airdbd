import React, { Component } from "react";
import "./banner.css";
import "bootstrap/dist/css/bootstrap.min.css";

// Banner component at the top of every page.
// Colored strip (or image) that stretches across
// the top of the page and has the page title inside.
// Child of all the page components: homePage, aboutPage, modelPage, instancePage
//
// Takes "img" attribute, which is the source of the image.
// Takes text, which will be displayed in the center as a title.
// Example: <banner img="images/example_image.png">Example Banner Text</banner>

class banner extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div>
        <div
          className="jumbotron jumbotron-fluid text-center"  
          style={{
            backgroundImage: this.props.img
          }}
        >
          <h1 className="display-1 text-white" id="subtitle">{this.props.title}</h1>
          <h3 className="text-white">{this.props.subTitle}</h3>
        </div>
      </div>
    );
  }
}

export default banner;
