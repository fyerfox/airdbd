import React from "react";
import { expect } from "chai";
import { shallow } from "enzyme";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Banner } from "../";

Enzyme.configure({ adapter: new Adapter() });

describe("<Banner />", () => {
  it("renders title correctly", () => {
    const wrapper = shallow(<Banner title="banner" />);
    expect(wrapper.text()).to.equal("banner");
  });

  it("renders sub title correctly", () => {
    const wrapper = shallow(<Banner subTitle="sub" />);
    expect(wrapper.text()).to.equal("sub");
  });
});
