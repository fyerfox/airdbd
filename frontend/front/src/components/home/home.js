import React, { Component } from "react";
import { Banner } from "../../components";
import { LinkContainer } from "react-router-bootstrap";
import "./home.css";
import Airplane from "../../pics/home_airplane.jpg";
import Airport from "../../pics/home_airport.jpg";
import City from "../../pics/home_city.jpg";
import Flight from "../../pics/home_flight.jpg";
import AirplaneType from "../../pics/home_airplaneType.jpg";
import SearchBar from "../search/searchBar"

// Contents of the home page/splash page.
// Contains a banner and several subsections
// for each model.
// Child of App component, hot-swapped during navigation.

class homePage extends Component {
  render() {
    return (
      <div>
        {/* Banner Component */}
        <Banner
          title="Air deciBel Database"
          subTitle="Live quietly"
        />
        {/* Content Here */}
        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-4  text-left">
              <h2 className="display-3">
                <strong>Cities</strong>
              </h2>
              <h4>
                We give almost every city in the U.S. a score according to their
                air noises.
              </h4>
              <p>Want to know which city is quiet?</p>
              <LinkContainer to="/cities">
                <button className="btn btn-default btn-lg button-blue" id="btnCity">
                  Find it out
                </button>
              </LinkContainer>
            </div>
            <div className="col-sm-8 ">
              <img className="left-pic shadow-lg rounded" src={City} />
            </div>
          </div>
        </div>

        <div className="container-fluid bg-blue">
          <div className="row">
            <div className="col-sm-8">
              <img src={Flight} className="right-pic shadow-lg rounded" />
            </div>
            <div className="col-sm-4  text-left">
              <h2 className="display-3">
                <strong>Flights</strong>
              </h2>
              <h4>
                We track over hundreds of thousands flights with a highly
                reliable database.
              </h4>
              <p>How much noise were made by the flight you took?</p>
              <LinkContainer to="/flights">
                <button className="btn btn-default btn-lg button-white" id="btnFlight">
                  Take a look
                </button>
              </LinkContainer>
            </div>
          </div>
        </div>

        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-4  text-left">
              <h2 className="display-3">
                <strong>Airports</strong>
              </h2>
              <h4>
                We collect very detailed information of each airport in the U.S.
              </h4>
              <p>Want to know how busy is your city's airport?</p>
              <LinkContainer to="/airports">
                <button className="btn btn-default btn-lg button-blue" id="btnAirport">
                  Check it out
                </button>
              </LinkContainer>
            </div>
            <div className="col-sm-8">
              <img className="left-pic shadow-lg rounded" src={Airport} />
            </div>
          </div>
        </div>

        <div className="container-fluid bg-blue">
          <div className="row">
            <div className="col-sm-8">
              <img src={Airplane} className="right-pic shadow-lg rounded" />
            </div>
            <div className="col-sm-4  text-left">
              <h2 className="display-3">
                <strong>Airplanes</strong>
              </h2>
              <h4>
                We list all the individual instances of airplanes that are
                currently in the fleet.
              </h4>
              <p>Curious about how much noice a specific plane can make?</p>
              <LinkContainer to="/airplanes">
                <button className="btn btn-default btn-lg button-white" id="btnAirplane">
                  Have a look
                </button>
              </LinkContainer>
            </div>
          </div>
        </div>

        <div className="container-fluid">
          <div className="row">
            <div className="col-sm-4  text-left">
              <h2 className="display-3">
                <strong>Airplane types</strong>
              </h2>
              <h4>
                We provide information on each different type of aicraft used in
                the various airports
              </h4>
              <p>Wondering about the specifications of an airplane type?</p>
              <LinkContainer to="/airplanetypes">
                <button className="btn btn-default btn-lg button-blue" id="btnAirplaneType">
                  Click here!
                </button>
              </LinkContainer>
            </div>
            <div className="col-sm-8">
              <img className="left-pic shadow-lg rounded" src={AirplaneType} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default homePage;
