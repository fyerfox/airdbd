import React from "react";
import { expect } from "chai";
import { shallow, mount, render } from "enzyme";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Home, Banner } from "../";

Enzyme.configure({ adapter: new Adapter() });

describe("Home.js", () => {
  it("renders banner correctly", () => {
    const wrapper = shallow(<Home />);
    expect(
      wrapper.contains(
        <Banner
          title="Air deciBel Database"
          subTitle="Live quietly"
        />
      )
    ).to.equal(true);
  });

  it("renders models correctly", () => {
    const wrapper = shallow(<Home />);
    expect(wrapper.find("h2").length).to.equal(5);
  });

  it("renders bottoms correctly", () => {
    const wrapper = shallow(<Home />);
    expect(wrapper.find("button").length).to.equal(5);
  });
});
