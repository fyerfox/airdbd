import React, { Component } from "react";
import Highlight from "react-highlighter";
import { Banner } from "../";
import { Link } from "react-router-dom";
//const fetch = require("node-fetch");
import SearchBar from "./searchBar";

// Child of App component, hot-swapped during navigation.

class searchSite extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: "searching...",
      resultCount: "0",
      searchText: this.props.match.params.search,
      resultType: [],

      City: [
        [
          { title: "Country", width: 2, fontSize: 20 },
          { title: "Score", width: 2, fontSize: 20 },
          { title: "City name", width: 2, fontSize: 20 }
        ]
      ],

      Flight: [
        [
          { title: "Flight ICAO number", width: 2, fontSize: 20 },
          { title: "Airline IATA number", width: 2, fontSize: 20 },
          { title: "Aircraft", width: 2, fontSize: 20 },
          { title: "Status", width: 2, fontSize: 20 }
        ]
      ],

      Airport: [
        [
          { title: "Airport name", width: 2, fontSize: 20 },
          { title: "ICAO code", width: 2, fontSize: 20 },
          { title: "Lat", width: 2, fontSize: 20 },
          { title: "Long", width: 2, fontSize: 20 },
          { title: "Country", width: 2, fontSize: 20 },
          { title: "City name", width: 2, fontSize: 20 }
        ]
      ],

      AircraftType: [
        [
          { title: "Production_name", width: 2, fontSize: 20 },
          { title: "IATA code", width: 2, fontSize: 20 },
          { title: "Noise_level", width: 2, fontSize: 20 }
        ]
      ],

      Aircraft: [
        [
          { title: "Registration number", width: 4, fontSize: 20 },
          { title: "Aircraft type", width: 2, fontSize: 20 },
          { title: "IATA type", width: 2, fontSize: 20 },
          { title: "Model code", width: 2, fontSize: 20 }
        ]
      ]
    };
  }

  displayResult() {
    if (this.state.status != "searching..." && this.state.resultCount != 0) {
      let out = [];
      var i;
      for (i = 0; i < this.state.resultType.length; i++) {
        var titleRow = this.state[this.state.resultType[i]][0].map(
          (col, index) => (
            <div key={index} className={"col-sm-" + col.width}>
              <p style={{ fontSize: "20px", color: col.color || "black" }}>
                <strong>{col.title}</strong>
              </p>
            </div>
          )
        );
        // Construct the rest of the rows, with instance data
        console.log(this.state[this.state.resultType[i]]);
        var dataRows = this.state[this.state.resultType[i]]
          .slice(1)
          .map((instance, rowIndex) => (
            <Link to={instance[0]} key={rowIndex} id={rowIndex}>
              <div className={"row" + (rowIndex % 2 ? " bg-grey" : "")}>
                {instance.slice(1).map((attrValue, colIndex) => (
                  <div
                    className={
                      "col-sm-" +
                      this.state[this.state.resultType[i]][0][colIndex].width
                    }
                    key={colIndex}
                  >
                    <p
                      style={{
                        fontSize:
                          this.state[this.state.resultType[i]][0][colIndex]
                            .fontSize || "30px",
                        color:
                          this.state[this.state.resultType[i]][0][colIndex]
                            .color || "black"
                      }}
                    >
                      <Highlight search={this.state.searchText}>
                        {attrValue}
                      </Highlight>
                    </p>
                  </div>
                ))}
              </div>
            </Link>
          ));

        out.push(
          <div>
            <h1 align="left" style={{ paddingLeft: "100px" }}>
              {this.state.resultType[i]}
            </h1>
            <div className="container-fluid">
              <div className="row bg-grey">{titleRow}</div>
              {dataRows}
            </div>
          </div>
        );
      }

      return out;
    }
  }

  doApiRequest(search) {
    // Do API call
    fetch("https://www.airdbd.net/api/search/?format=json&search=" + search)
      .then(res => res.json())
      .then(data => this.appendData(data, search));
  }

  appendData(data, search) {
    let new_state = this.state;
    new_state.searchText = search;

    // Empty out the results from the previous search, if any
    new_state.resultType = [];
    new_state.City = new_state.City.slice(0, 1);
    new_state.Flight = new_state.Flight.slice(0, 1);
    new_state.Aircraft = new_state.Aircraft.slice(0, 1);
    new_state.AircraftType = new_state.AircraftType.slice(0, 1);
    new_state.Airport = new_state.Airport.slice(0, 1);

    // Iterate over the airplane instances in data.aren’taren’taren’taren’taren’tresults
    // and append each one's data to new_rows
    for (const instance of data.results) {
      let row = [];

      //Add found types
      if (new_state.resultType.indexOf(instance.type) < 0) {
        new_state.resultType.push(instance.type);
      }

      //Make it eaiser for linking
      let t = instance.type;
      if (t == "Aircraft") {
        t = "airplane";
      } else if (t == "AircraftType") {
        t = "airplanetype";
      }
      t = t.toLowerCase();

      //Add other data
      for (var k in instance.obj) {
        if (k == "id") {
          row.push("/" + t + "/" + instance.obj[k]);
        } else if (k == "position") {
          row.push(instance.obj[k]["latitude"]);
          row.push(instance.obj[k]["longitude"]);
        } else if (k != "timezone") {
          row.push(instance.obj[k]);
        }
      }
      new_state[instance.type].push(row);
    }

    if (data.count == 1) {
      new_state.status = data.count + " result found";
    } else {
      new_state.status = data.count + " results found";
    }

    new_state.resultCount = data.count;
    this.setState(new_state);
  }

  componentDidMount() {
    this.doApiRequest(this.props.match.params.search);
  }

  handleSearch = search => {
    this.props.history.push("/search/" + search);
    this.setState({
      status: "searching...",
      searchText: search
    });
    this.doApiRequest(search);
  };

  render() {
    return (
      <div>
        {/* Banner Component */}
        <Banner
          title={"Searching for " + this.state.searchText}
          subTitle={this.state.status}
        />

        {/* Content Here */}
        <div>{this.displayResult()}</div>
      </div>
    );
  }
}

export default searchSite;
