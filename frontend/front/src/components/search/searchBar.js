import React, { Component } from "react";

// Search bar at the top of model pages and home page


class SearchBar extends Component {

  state = {
    // The current value inside the search bar
    textFieldValue: ""
  };


  /*  Event handler for when text is changed
   */
  handleChange = (event) => {
    this.setState({ textFieldValue: event.target.value })
  }


  /*  Event handler for when the search is submitted
   */
  handleSubmit = (event) => {
    // Get the value from the text box and issue a search
    var search = this.state.textFieldValue
    this.props.onSearch(search)
    if (!this.props.refresh)
      event.preventDefault()
  }


  render() {

    return (
        <div>
            {/* Text box for search text */}
            <form onSubmit={ this.handleSubmit }>
                <div class="md-form mt-0">
                    <input class="form-control" placeholder="Search" type="text" size="12" id="searchBox" onChange={ this.handleChange }/>
                </div>
            </form>
        </div>
    );
  }
}

export default SearchBar;
