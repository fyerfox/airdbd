import React, { Component } from "react";
import ModelPage from "./model.js"
import assert from "assert"

// The content of the city model page.

class CityModelPage extends ModelPage {

    constructor (props) {
        super (props) 

        this.state.rows = [[
            // Column titles and widths for instance table
            { title: "Image",
              width: 2 },

            { title: "City",
              width: 3,
              sortable: true,
              filterable: true },

            { title: "Country",
              width: 2,
              sortable: true,
              filterable: true },

            { title: "Latitude",
              width: 2,
              fontSize: 20,
              sortable: true,
              filterable: true,
              numerical: true },

            { title: "Longitude",
              width: 2,
              fontSize: 20,
              sortable: true,
              filterable: true,
              numerical: true },

            { title: "Score",
              width: 1,
              color: "red",
              sortable: true,
              filterable: true,
              numerical: true }
          ]]

        this.api_url_modelname = "cities"
        this.banner_title = "Cities"
        this.banner_subtitle = "Noise rating for cities worldwide."
        this.instance_table_id = "tblCities"

        this.createInstanceTableRow = (instance) => {
            let row = []

            row.push ("city/" + instance.id)       // URL of instance page

            row.push (<img src={ instance.images ?
                JSON.parse(instance.images.replace (/'/g, "\""))[0]  // Image source
                : "X" } width="80" />)

            row.push (instance.city_name)          // City name
            row.push (instance.country)            // Country name

            row.push (instance.position ?
                instance.position.latitude
                : null)                            // Latitude

            row.push (instance.position ?
                instance.position.longitude
                : null)                            // Longitude

            row.push (instance.score ?
                instance.score.toFixed(2)
                : null)                            // Noise Level

            return row
        }

        this.attributeNameOfColumn = (column) => {
            switch (column) {
                case 1:
                    return "city_name"
                case 2:
                    return "country"
                case 3:
                    return "latitude"
                case 4:
                    return "longitude"
                case 5:
                    return "score"
                default:
                    assert (false)
            }
        }
    }
}

export default CityModelPage;
