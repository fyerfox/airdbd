import React from "react";
import { expect } from "chai";
import { shallow, mount, render } from "enzyme";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import {
  CityModel,
  AirplaneModel,
  AirportModel,
  AirplaneTypeModel,
  FlightModel
} from "..";
import Banner from "../banner/banner.js";
import InstanceTable from "./instanceTable.js";
import Paginator from "./paginator.js";
import SearchBar from "../search/searchBar.js";

Enzyme.configure({ adapter: new Adapter() });
const model = [
  shallow(<CityModel />),
  shallow(<AirplaneModel />),
  shallow(<AirportModel />),
  shallow(<AirplaneTypeModel />),
  shallow(<FlightModel />)
];
var i;
for (i = 0; i < model.length; i++) {
  describe(String(model[i]), () => {
    const wrapper = model[i];
    it("Initialize state correctly", () => {
      expect(wrapper.state().page).to.equal(0);
      expect(wrapper.state().search).to.equal(null);
      expect(wrapper.state().sortBy).to.equal(null);
      expect(wrapper.state().fetching).to.equal(true);
      expect(wrapper.state().instanceCount).to.equal(0);
    });

    it("contains paginator", () => {
      expect(wrapper.find(Paginator)).to.have.length(1);
    });

    it("contains SearchBar", () => {
      expect(wrapper.find(SearchBar)).to.have.length(1);
    });

    it("contains InstanceTable", () => {
      expect(wrapper.find(InstanceTable)).to.have.length(1);
    });

    it("contains Banner", () => {
      expect(wrapper.find(Banner)).to.have.length(1);
    });
  });
}
