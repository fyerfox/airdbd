import React, { Component } from "react";
import Banner from "../banner/banner.js"
import InstanceTable from "./instanceTable.js"
import Paginator from "./paginator.js"
import SearchBar from "../search/searchBar.js"
import assert from "assert"

// Parent class for every model page
// Contains a banner with the title of the
// page, and a table of instances.
// Child of App component, hot-swapped during navigation.

class ModelPage extends Component {

  state = {
    // Index of the page we're currently viewing
    page: 0,

    // Current search
    search: null,

    // Current attribute to sort by
    sortBy: null,

    // Sorting is descending?
    desc: false,

    // Name of attribute to filter by
    filterBy: null,

    // Relation of filter (=, <, or >)
    filterRelation: null,

    // Value for filter to compare
    filterValue: null,

    // Total number of instances in database
    // Initialized by API call
    instanceCount: 0,

    // Is the data currently loading?
    fetching: true,
 
    // Data for instance table
    // 2d array of data to be passed to a new instanceTable
    rows: [[]]
  };


  // ----------------------------------------------------

  /*  Properties of a model type
   *  All subclasses must redefine these
   */

  // name of model in api url
  api_url_modelname = ""

  banner_title = ""         // page title
  banner_subtitle = ""      // page subtitle
  instance_table_id = ""    // id of instanceTable

  // function to convert api data to a row in the InstanceTable
  createInstanceTableRow = (instance) => []

  // function to convert column index to an attribute name
  attributeNameOfColumn = (column) => ""

  // ----------------------------------------------------

  
  /*  React lifecycle hook called when the component is first mounted
   */
  componentDidMount () {
    this.doApiRequest(0, null, null, false, null, null, null)
  }


  /*  Issues an AirdBD API request with the given parameters
   */
  doApiRequest (pageNum, search, sortBy, desc, filterBy, filterRelation, filterValue) {
    // Do API call
    // Fetch with a limit of 10 instances
    let url = ('https://www.airdbd.net/api/' + this.api_url_modelname +
                '/?format=json&limit=10&offset=' + (pageNum * 10))
    if (search) {
        url += "&search=" + search
    }
    if (sortBy) {
       url += "&order_by=" + (desc ? "-" : "") + sortBy
    }
    if (filterBy) {
        let param =  filterBy
        if (filterRelation === "<") {
            param += "__lt"
        } else if (filterRelation === ">") {
            param += "__gt"
        }
        url += "&" + param + "=" + filterValue
    }

    this.setState ({ fetching: true })
    fetch (url)
        .then(res => res.json())
        .then(data => this.appendData(data, pageNum, search, sortBy, desc, filterBy, filterRelation, filterValue))
  }


  /* This function is called after an API fetch returns.
   * The argument "data" is the json data returned by the fetch
   * (converted to a JS object).
   */
  appendData = (data, pageNum, search, sortBy, desc, filterBy, filterRelation, filterValue) => {
    // Make a local mutable copy of the state
    let new_rows = this.state.rows.slice(0, 1)

    // Iterate over the airplane instances in data.results
    // and append each one's data to new_rows
    for (const instance of data.results) {
        var row = this.createInstanceTableRow(instance)
        new_rows.push (row)
    }

    // Update the new state
    this.setState({ instanceCount:  data.count,
                    rows:           new_rows,
                    page:           pageNum,
                    search:         search,
                    sortBy:         sortBy,
                    desc:           desc,
                    filterBy:       filterBy,
                    filterRelation: filterRelation,
                    filterValue:    filterValue,
                    fetching:       false })
  }

 /*  Handler for the paginator to change pages.
   *  The paginator will pass the new page number, and all
   *  we have to do is update our state to refresh the component.
   */
  handlePageChange = (newPageNum) => {
    this.doApiRequest( newPageNum,
                       this.state.search,
                       this.state.sortBy,
                       this.state.desc,
                       this.state.filterBy,
                       this.state.filterRelation,
                       this.state.filterValue )
  }

  /* Handler for when text is entered into the search bar
   */
  handleSearch = (search) => {
    this.doApiRequest( this.state.page,
                       search,
                       this.state.sortBy,
                       this.state.desc,
                       this.state.filterBy,
                       this.state.filterRelation,
                       this.state.filterValue )
  }

  /* Handler for when a sorting button is pressed
   */
  handleSort = (buttonColumn, desc) => {
    var attribute = this.attributeNameOfColumn (buttonColumn)
    assert (attribute != null)
    this.doApiRequest ( this.state.page,
                        this.state.search,
                        attribute,
                        desc,
                        this.state.filterBy,
                        this.state.filterRelation,
                        this.state.filterValue )
  }

  /*  Handler for when a filter is entered
   */
  handleFilter = (columnIndex, relation, value) => {
    var attribute = this.attributeNameOfColumn (columnIndex)
    assert (attribute != null)
    this.doApiRequest ( this.state.page,
                        this.state.search,
                        this.state.sortBy,
                        this.state.desc,
                        attribute,
                        relation,
                        value )

  }

  render() {
    return (
        <div>
            {/* Banner Component */}
            <Banner
                title={ this.banner_title }
                subTitle={ this.banner_subtitle }
                img="X" />

            {/* Search Bar */}
            <SearchBar
                onSearch={ this.handleSearch } />

            { this.state.fetching ? "Fetching..." : null }

            {/* Table of Instances */}
            <InstanceTable
                rows={ this.state.rows }
                id={ this.instance_table_id }
                search={ this.state.search }
                onSort={ this.handleSort }
                onFilter={ this.handleFilter }/>

            {/* Paginator Buttons*/}
            <Paginator
                currentPage= { this.state.page }
                totalPages=  { Math.ceil(this.state.instanceCount / 10)}
                onPageChange={ this.handlePageChange } />
        </div>
    );
  }
}

export default ModelPage;
