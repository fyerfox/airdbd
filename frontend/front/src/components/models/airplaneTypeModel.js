import React, { Component } from "react";
import ModelPage from "./model.js"
import assert from "assert"

// The content of the city model page.

class AirplaneTypeModelPage extends ModelPage {

    constructor (props) {
        super (props) 

        this.state.rows = [[
            // Column titles and widths for instance table
            { title: "Image",
              width: 2 },

            { title: "Model Name",
              width: 5,
              fontSize: 18,
              sortable: true,
              filterable: true },

            { title: "IATA Code",
              width: 1,
              fontSize: 20,
              sortable: true,
              filterable: true },

            { title: "#",
              width: 1,
              fontSize: 20,
              sortable: true,
              filterable: true,
              numerical: true },

            { title: "%",
              width: 1,
              fontSize: 20,
              sortable: true,
              filterable: true,
              numerical: true },

            { title: "dB",
              width: 2,
              fontSize: 30,
              color: "red",
              sortable: true,
              filterable: true,
              numerical: true }
         ]]

        this.api_url_modelname = "aircrafttype"
        this.banner_title = "Airplane Types"
        this.banner_subtitle = ("Specifications on models of airplanes, " +
                                "such as decibel rating.")
        this.instance_table_id = "tblAirplaneTypes"

        this.createInstanceTableRow = (instance) => {
            let row = []

            row.push ("airplanetype/" + instance.id)    // URL of instance page

            row.push (<img src={ instance.images ?
                JSON.parse(instance.images.replace (/'/g, "\""))[0]  // Image source
                : "X" } width="80" />) 

            row.push (instance.production_name || "-")  // Name of airplane
            row.push (instance.iata_code       || "-")  // IATA code
            row.push (instance.num_aircraft    || "-")  // Number in operation

            row.push (instance.percentage ?
                (instance.percentage * 100).toFixed(3)
                + "%" : "-")                            // Percentage in operation

            row.push (instance.noise_level     || "-")  // Noise Level

            return row
        }

        this.attributeNameOfColumn = (column) => {
            switch (column) {
                case 1:
                    return "production_name"
                case 2:
                    return "iata_code"
                case 3:
                    return "num_aircraft"
                case 4:
                    return "percentage"
                case 5:
                    return "noise_level"
                default:
                    assert (false)
            }
        }
    }
}

export default AirplaneTypeModelPage;
