import React, { Component } from "react";
import { Link } from "react-router-dom"
import Highlight from "react-highlighter"

// Table of instances of a certain model.
// Vertical table where each row is an instance,
// and the contents of the row are some attributes.
// Used in instance and model pages.

// Data passed down with "rows" attribute, like so:
// <instanceTable rows={ js_array } />
// The value is expected to be a rectangular 2d array.
// The first row of the array should be a "title" row,
// where each element describes the title and width of the row.
// The sum of all widths must NOT exceed 12.
// Each subsequent element should contain the data for one row.
// The first element of the row is the url of a link to the instance.

// Example of an acceptable value of "rows":
// (results in a table with 2 rows and 3 columns)
// [ [ { title: "Name",  width: 4 },
//     { title: "Image", width: 4 },
//     { title: "Age",   width: 4 } ],
//   [ "www.example.com/john", "John", <img src="./john.png"/>, 24 ],
//   [ www.example.com/amy",   "Amy",  <img src="./amy.png" />, 34 ] ]


class InstanceTable extends Component {

  state = {

    // State of the filter in each column.
    // For each column, keep track of the relation (=, <, or >) and value.
    filterStates: this.props.rows[0].map ( (col, index) => ["=", ""])
  }

  handleFilterChange = (colIndex, event) => {
    let newFilterStates = this.state.filterStates

    // If the change was in the text field, update the value.
    if (event.target.tagName == "INPUT") {
        newFilterStates[colIndex][1] = event.target.value
    }

    // If the change was in the dropdown menu, update the relation.
    else if (event.target.tagName == "SELECT") {
        newFilterStates[colIndex][0] = event.target.value
    }

    this.setState({ filterStates: newFilterStates })
    console.log (this.state.filterStates)
  }

  handleFilterSubmit = (colIndex, event) => {
    this.props.onFilter ( colIndex,
                          this.state.filterStates[colIndex][0],
                          this.state.filterStates[colIndex][1] )
    event.preventDefault()
  }


  render() {

    // Construct title row from first array inside props.rows
    var titleRow = this.props.rows[0].map ((col, index) =>
                (<div key={index} className={"col-sm-" + col.width}>

                    {/* Header for column */}
                    <h3 style={{ fontSize:  "20px",
                                color:     col.color || "black",
                                textAlign: "left" }}>
                        <strong>{col.title}</strong>
                    </h3>

                    <div className="container-fluid">

                        {/* Ascending sort button */}
                        { col.sortable ? (
                        <div className="row">
                            <button
                              className="btn btn-light"
                              style={{ fontSize: 10,
                                       padding:  3 }}
                              onClick={() => this.props.onSort(index, false)}>
                                {"/\\"}
                            </button>
                        </div>) : null }

                        {/* Descending sort button */}
                        { col.sortable ? (
                        <div className="row">
                            <button
                              className="btn btn-light"
                              style={{ fontSize: 10,
                                       padding:  3 }}
                              onClick={() => this.props.onSort(index, true)}>
                                {"\\/"}
                            </button>
                        </div>) : null }

                        {/* Filter Widgets*/}
                        { col.filterable ? (
                            <form onSubmit={ (event) => this.handleFilterSubmit(index, event) }>
                                <select onChange={ (event) => this.handleFilterChange(index, event) }>
                                    <option value="=">{"="}</option>
                                    { col.numerical ? (<>
                                    <option value=">">{">"}</option>
                                    <option value="<">{"<"}</option>
                                    </>) : null }
                                </select>

                                <input type="text" size="5" onChange={ (event) => this.handleFilterChange(index, event) } />
                                <input type="submit" className="btn-info" value="apply" />
                            </form>

                        ) : null }

                    </div>
                </div>))

    // Construct the rest of the rows, with instance data
    var dataRows = this.props.rows.slice(1).map ((instance, rowIndex) =>
                (<Link to={instance[0]} key={rowIndex} id={rowIndex}>
                  <div className={"row" + (rowIndex % 2 ? " bg-grey" : "")}>
                    {instance.slice(1).map ((attrValue, colIndex) =>
                        (<div className={"col-sm-" + this.props.rows[0][colIndex].width} key={colIndex}>
                            <p style={{ fontSize: this.props.rows[0][colIndex].fontSize || "30px",
                                        color: this.props.rows[0][colIndex].color || "black" }}>
                                <Highlight search={ this.props.search} >
                                    {attrValue}
                                </Highlight>
                            </p>
                        </div>))}
                  </div>
                </Link>))

    return (
        <div className="container-fluid">
            <div className="row bg-grey">
                { titleRow }
            </div>

            { dataRows }

        </div>
    );
  }
}

export default InstanceTable;

