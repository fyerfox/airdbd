import React, { Component } from "react";

// Component at the bottom of a model page that
// cycles through page numbers in response to button presses.

class Paginator extends Component {

  state = {
    // The current value inside the "go to page" input field
    textFieldValue: ""
  };

  /*  Event handlers for "go to page"
   *  text input field
   */
  handleChange = (event) => {
    this.setState({ textFieldValue: event.target.value })
  }

  handleSubmit = (event) => {
    // Get the value from the text box and validate
    // that it is a correct page number, then
    // navigate to that page
    var newPageNum = +(this.state.textFieldValue)
    if (!isNaN(newPageNum) && newPageNum > 0 && newPageNum <= this.props.totalPages) {
        this.props.onPageChange(newPageNum - 1)
    } else {
        alert ("Not a valid page number: " + this.state.textFieldValue)
    }
    event.preventDefault()
  }

  render() {

    // Determine which page numbers to display
    var pageNumbers = [...Array(this.props.totalPages).keys()]
    pageNumbers = [...pageNumbers.map((n, index) => (
            <button
              key={index}
              className="btn-default"
              onClick={ () => this.props.onPageChange(n)}>
                {n + 1}
            </button>
        ))]

    // Limit the numbers to 6 on screen at a time
    if (pageNumbers.length > 6) {
        var startIndex = Math.max (this.props.currentPage - 3, 0)
        var endIndex = Math.min (startIndex + 6, pageNumbers.length)
        pageNumbers = pageNumbers.slice (startIndex, endIndex)
    }

    return (
        <div>
            {/* Previous button */}
            <button
              className="btn-default"
              onClick={ () => this.props.onPageChange(Math.max(this.props.currentPage - 1, 0))}>
                {"<"}
            </button>

            {/* First button */}
            <button
              className="btn-default"
              onClick={ () => this.props.onPageChange(0)}>
                {"<<"}
            </button>

            {/* Number Buttons */}
            { pageNumbers } 

            {/* Last button */}
            <button
              className="btn-default"
              onClick={ () => this.props.onPageChange(this.props.totalPages - 1)}>
                {">>"}
            </button>

            {/* Next button */}
            <button
              className="btn-default"
              onClick={ () => this.props.onPageChange(Math.min(this.props.currentPage + 1, this.props.totalPages - 1))}>
                {">"}
            </button>

            {/* Show the current page */}
            <p>page {this.props.currentPage + 1} of {this.props.totalPages}</p>

            {/* Text box for direct page navigation */}
            <form onSubmit={ this.handleSubmit }>
                go to page:
                <input type="text" size="5" onChange={ this.handleChange }/>
                <input type="submit" className="btn-info" value="go" />
            </form>
        </div>
    );
  }
}

export default Paginator;
