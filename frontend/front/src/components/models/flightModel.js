import React, { Component } from "react";
import ModelPage from "./model.js"
import assert from "assert"

// The content of the flights model page.

class FlightModelPage extends ModelPage {

    constructor (props) {
        super (props) 

        this.state.rows = [[
            // Column titles and widths for instance table
            { title: "#",
              width: 1,
              fontSize: 17,
              sortable: true,
              filterable: true },

            { title: "Source",
              width: 3,
              fontSize: 20,
              sortable: true,
              filterable: true },

            { title: "Destination",
              width: 4,
              fontSize: 20,
              sortable: true,
              filterable: true },

            { title: "Status",
              width: 2,
              fontSize: 15,
              sortable: true,
              filterable: true },

            { title: "Airline #",
              width: 2,
              fontSize: 15,
              sortable: true,
              filterable: true }
          ]]

        this.api_url_modelname = "flights"
        this.banner_title = "Flights"
        this.banner_subtitle = "Airplane flights we've tracked."
        this.instance_table_id = "tblFlights"

        this.createInstanceTableRow = (instance) => {
            let row = []

            row.push ("flight/" + instance.id)                      // URL of instance page
            row.push (instance.flight_icao_number || "-")           // Flight Number

            row.push (instance.src && instance.src.airport_name ?
                instance.src.airport_name : "-")                    // Source

            row.push (instance.dst && instance.dst.airport_name ?
                instance.dst.airport_name : "-")                    // Destination

            row.push (instance.status || "-")                       // Flight status
            row.push (instance.airline_iata_number || "-")          // Airline number

           return row
        }

        this.attributeNameOfColumn = (column) => {
            switch (column) {
                case 0:
                    return "flight_icao_number"
                case 1:
                    return "src__airport_name"
                case 2:
                    return "dst__airport_name"
                case 3:
                    return "status"
                case 4:
                    return "airline_iata_number"
                default:
                    assert (false)
            }
        }
    }
}

export default FlightModelPage;
