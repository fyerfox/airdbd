import React, { Component } from "react";
import ModelPage from "./model.js"
import assert from "assert"

// The content of the city model page.

class AirplaneModelPage extends ModelPage {

    constructor (props) {
        super (props) 

        this.state.rows = [[
            // Column titles and widths for instance table
            { title: "Registration Number",
              width: 4,
              fontSize: 20,
              sortable: true,
              filterable: true },

            { title: "IATA Type",
              width: 2,
              fontSize: 20,
              sortable: true,
              filterable: true },

            { title: "Model Code",
              width: 2,
              fontSize: 20,
              sortable: true,
              filterable: true },

            { title: "Engine Count",
              width: 2,
              fontSize: 20,
              sortable: true,
              filterable: true,
              numerical: true },

            { title: "Engine Type",
              width: 2,
              fontSize: 20,
              sortable: true,
              filterable: true }
          ]]

        this.api_url_modelname = "aircraft"
        this.banner_title = "Airplanes"
        this.banner_subtitle = "Identification information for individual airplanes."
        this.instance_table_id = "tblAirplanes"

        this.createInstanceTableRow = (instance) => {
            let row = []

            row.push ("airplane/" + instance.id)            // URL of instance page
            row.push (instance.registration_number || "-")  // Registration number
            row.push (instance.IATA_type           || "-")  // IATA Type
            row.push (instance.model_code          || "-")  // Model Code
            row.push (instance.engine_count        || "-")  // Engine Count
            row.push (instance.engine_type         || "-")  // Engine Type

            return row
        }

        this.attributeNameOfColumn = (column) => {
            switch (column) {
                case 0:
                    return "registration_number"
                case 1:
                    return "IATA_type"
                case 2:
                    return "model_code"
                case 3:
                    return "engine_count"
                case 4:
                    return "engine_type"
                default:
                    assert (false)
            }
        }
    }
}

export default AirplaneModelPage;
