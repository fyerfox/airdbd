import React, { Component } from "react";
import ModelPage from "./model.js"
import assert from "assert"

// The content of the city model page.

class AirportModelPage extends ModelPage {

    constructor (props) {
        super (props) 

        this.state.rows = [[
            // Column titles and widths for instance table
          { title: "Image",
              width: 2 },

            { title: "Airport",
              width: 3,
              fontSize: 20,
              sortable: true,
              filterable: true },

            { title: "Abbv.",
              width: 1,
              fontSize: 25,
              sortable: true,
              filterable: true },

            { title: "Country",
              width: 2,
              fontSize: 20,
              sortable: true,
              filterable: true },

            { title: "Lat",
              width: 2,
              fontSize: 20,
              sortable: true,
              filterable: true,
              numerical: true },

            { title: "Long",
              width: 2,
              fontSize: 20,
              sortable: true,
              filterable: true,
              numerical: true }
        ]]

        this.api_url_modelname = "airports"
        this.banner_title = "Airports"
        this.banner_subtitle = "Listing of airports in the world."
        this.instance_table_id = "tblAirports"

        this.createInstanceTableRow = (instance) => {
            let row = []

            row.push ("airport/" + instance.id)  // URL of instance page
            row.push ("X")                       // Image Source (TODO)
            row.push (instance.airport_name)     // Name of airport
            row.push (instance.icao_code)        // Airport Abbreviation
            row.push (instance.country)          // Country

            if (instance.position) {
                row.push (instance.position.latitude)   // Latitude
                row.push (instance.position.longitude)  // Longitude
            }

            return row
        }

        this.attributeNameOfColumn = (column) => {
            switch (column) {
                case 1:
                    return "airport_name"
                case 2:
                    return "icao_code"
                case 3:
                    return "country"
                case 4:
                    return "latitude"
                case 5:
                    return "longitude"
                default:
                    assert (false)
            }
        }
    }
}

export default AirportModelPage;
