import React from "react";
import { expect } from "chai";
import { shallow } from "enzyme";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Footer } from "..";

Enzyme.configure({ adapter: new Adapter() });

describe("<Footer />", () => {
  it("renders text correctly", () => {
    const wrapper = shallow(<Footer />);
    expect(
      wrapper.contains(
        <p>&copy; AirdBd, Project of CS373 Fall 2019. All Rights Reserved.</p>
      )
    ).to.equal(true);
  });
});
