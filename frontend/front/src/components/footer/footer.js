import React, { Component } from "react";
import "./footer.css";
import { animateScroll as scroll } from "react-scroll";

// Component at the bottom of every page.
// Has a link to the top of the page, and other info.
// Child of App component.

class AirdBDFooter extends Component {
  ScrollTo() {
    scroll.scrollToTop();
  }
  render() {
    return (
      <div>
        {/* Content Here */}
        <footer className="container-fluid text-center">
          <a onClick={this.ScrollTo} className="move-top" href="#">
            <span className="glyphicon glyphicon-chevron-up"></span>
          </a>
          <p>&copy; AirdBd, Project of CS373 Fall 2019. All Rights Reserved.</p>
        </footer>
      </div>
    );
  }
}

export default AirdBDFooter;
