import React, { Component } from "react";
import * as d3 from "d3";

import mapData from "./worldMapData.geo.json";

// D3 visualization component
// Child of VisualizationPage component

// World heatmap of city scores
// Cities plotted on a world map with size/color
// representing their score

class ScoreHeatmapVisualization extends Component {

    componentDidMount () {
        this.doApiRequest();
    }

    doApiRequest () {
        let url = "https://www.airdbd.net/api/cities/?limit=4500"
        fetch (url)
            .then (res => res.json())
            .then (data => this.appendData (data));
    }

    appendData (data) {
        // Store lat, long, & score of each city in this.state.data
        let cityData = data.results
            .map (instance => {
                return {
                    latitude: instance.position.latitude,
                    longitude: instance.position.longitude,
                    score: instance.score
                }})
            .sort ((a, b) => a.score - b.score)
        this.drawMap (cityData);
    }

    drawMap (cityData) {
        // Find svg element
        var canvas = d3.select("body").select("#scoreHeatmap");

        // draw blue background
        canvas.append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("height", 450)
            .attr("width", 660)
            .attr("fill", "steelblue");

        // Create a new group ("g" element) for every
        // country in mapData.features
        var countryGroups = canvas.selectAll("g")
            .data (mapData.features)
            .enter()
            .append ("g");

        // Create a path generator
        var projection = d3.geoMercator()
            .scale(105)
            .translate([330, 315]);
        var path = d3.geoPath().projection(projection);

        // Create a new path in each group for each country group
        var areas = countryGroups.append ("path")
            .attr("d", path)
            .attr("fill", "lightgray");

        var colorScale = d3.scaleLinear()
            .domain([0, 3.5, 7])
            .range(["green", "yellow", "red"])

        // Draw a dot for each city
        var dots = canvas.append ("g")
            .selectAll ("circle")
            .data (cityData)
            .enter()
                .append ("circle")
                .attr ("cx",   d => projection([d.longitude, d.latitude])[0])
                .attr ("cy",   d => projection([d.longitude, d.latitude])[1])
                .attr ("r",    d => d.score * 0.3 + 1)
                .attr ("fill", d => colorScale(d.score) );
    }

    render() {
        return (
            <div>
                <svg
                  id="scoreHeatmap"
                  height="450"
                  width="660" >
                    <text
                      x="280"
                      y="220"
                      textLength="100">
                        Loading... 
                    </text>
                </svg>
            </div>
        )
    }
}

export default ScoreHeatmapVisualization;

