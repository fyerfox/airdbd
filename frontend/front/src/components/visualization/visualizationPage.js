import React, { Component } from "react"
import Banner from "../banner/banner.js"
import ScoreHeatmapVisualization from "./scoreHeatmapVisualization.js"
import ScoreDistVisualization from "./scoreDistVisualization.js"
import PlaneNoiseVisualization from "./planeNoiseVisualization.js"
import QELawsVisualization from "./QELawsVisualization.js"
import QEMainEmissionVisualization from "./QEMainEmissionVisualization.js"
import QEStatesVisualization from "./QEStatesVisualization.js"

// Page of visualizations accessible via navbar.
// Child of app component, hot-swapped during navigation.

class VisualizationPage extends Component {
    render () {
        return (
            <div>

                {/* Banner */}
                <Banner
                    title="Visualizations"
                    subTitle="D3 visualizations of our data and the data of our developer's site." />

                {/* AirdBD Visualization 1 */}
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-8">
                            <ScoreHeatmapVisualization />
                        </div>
                        <div className="col-sm-4" style={{"text-align" : "left"}}>
                            <h3> Noise Score Heatmap </h3>
                            <p>
                                This is a plot of every city in our database.
                                The size and color of each city are proportional
                                to its <em>noise score</em>. The noise score of a
                                city is a measurement of how often airplanes typically
                                pass overhead. The higher the score, the noisier the
                                city is.
                            </p>
                            <p>
                                In other words, this map shows which regions of the
                                world have the most air traffic. As you can see, the
                                vast majority of traffic is concentrated in North America
                                and Europe, with notable exceptions in:
                           </p>
                           <ul>
                                <li> Tokyo, Japan, </li>
                                <li> Hong Kong, China, </li>
                                <li> and Dubai, UAE, among others. </li>
                            </ul>
                        </div>
                    </div>
                </div>

                {/* AirdBD Visualization 2 */}
                <div className="container-fluid bg-grey">
                    <div className="row">
                        <div className="col-sm-4" style={{"text-align" : "left"}}>
                            <h3> Noise Score Histogram </h3>
                            <p>
                                This visualization uses the same data as the heatmap
                                above, but plots cities into a histogram by their noise
                                score. Each bar represents the number of cities whose
                                noise score is within a range.
                            </p>
                            <p>
                                For example, the rightmost bar indicates that <em>five </em>
                                cities have a score between 7.0 and 7.5. These five cities all
                                lie in the continental United States. They are, in order of
                                most to least noisy:
                            </p>
                            <ol>
                                <li> Chicago, IL </li>
                                <li> Newark, NJ </li>
                                <li> Linden, NJ </li>
                                <li> Queens, NY </li>
                                <li> Denver, CO </li>
                            </ol>
                        </div>
                        <div className="col-sm-8">
                            <ScoreDistVisualization />
                        </div>
                    </div>
                </div>

                {/* AirdBD Visualization 3 */}
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-8">
                            <PlaneNoiseVisualization />
                        </div>
                        <div className="col-sm-4" style={{"text-align" : "left"}}>
                            <h3> Airplane Types Scatterplot </h3>
                            <p>
                                This scatterplot shows the top 47 most common airplane
                                types in our database. On the horizontal axis is noise level,
                                a measurement of the amount of noise created by the
                                plane's engine. On the vertical axis is the number of instances
                                of that airplane type currently in operation -- in other words,
                                how popular the airplane is.
                            </p>
                            <p>
                                We can see that the two most common airplane types, the
                                <em> Airbus A318/A319/A32</em> and the <em>Boeing 737 NG</em>,
                                are also some of the most quiet. On the other hand, the
                                loudest plane, the <em>Boeing 747</em>, isn't used as frequently.
                            </p>
                        </div>
                    </div>
                </div>

                {/* Section Title */}
                <div className="container-fluid bg-grey">
                    <h2>{ "Quittin' Emittin' Visualizations "}</h2>
                    <a href="https://quittinemittin.me/">
                        Visit their site here.
                    </a>
                </div>

                {/* QE Visualization 1 */}
                <div className="container-fluid bg-grey">
                    <div className="row">
                        <div className="col-sm-4" style={{"text-align" : "left"}}>
                            <h3> Emissions and Laws Per Capita </h3>
                            <p>
                                This map has two color axes. The darkness of a state's color
                                shows the amount of emissions per capita (divided by population).
                                The purpleyness of a state's color shows the number of laws passed
                                in that state related to emissions.
                            </p>
                            <p>
                                While Texas has by far the most total emissions, Wyoming actually
                                has the highest emissions per capita, meaning Wyoming generates more
                                emissions per citizen than any other state. Texas appears to have the
                                most emission-related laws, indicated by its bright purple color.
                            </p>
                        </div>
                        <div className="col-sm-8">
                            <QEStatesVisualization />
                        </div>
                    </div>
                </div>

                {/* QE Visualization 2 */}
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-sm-8">
                            <QELawsVisualization />
                        </div>
                        <div className="col-sm-4" style={{"text-align" : "left"}}>
                            <h3> Laws Over Time </h3>
                            <p>
                                This is a graph of the number of emission-related laws per year.
                                On the X axis are the recent years up to 2019. On the Y axis is
                                the number of laws passed in a year.
                            </p>
                            <p>
                                As concern about climate change has mounted in recent years,
                                we've seen a sharp increase in the amount of legislation attempting
                                to limit emissions.
                            </p>
                        </div>
                    </div>
                </div>

                {/* QE Visualization 3 */}
                <div className="container-fluid bg-grey">
                    <div className="row">
                        <div className="col-sm-4" style={{"text-align" : "left"}}>
                            <h3> Most Prevalent Emission Types </h3>
                            <p>
                                Here we visualize the prevalence of each emission type.
                                The height of a bar shows the number of states in which an
                                emission type is most prevalent. In over half of US states,
                                petroleum is the largest contributor to emissions. Louisiana is
                                the only state whose emissions are primarily industrial.
                            </p>
                            <p>
                                A main takeaway from this graph is that petroleum-based vehicles
                                are the most common emission type in the US. Reducing their use by, for
                                example, limiting long-distance shipping and using more public transportation,
                                would have the biggest impact.
                            </p>
                        </div>
                        <div className="col-sm-8">
                            <QEMainEmissionVisualization />
                        </div>
                    </div>
                </div>

            </div>
        )
    }
}

export default VisualizationPage

