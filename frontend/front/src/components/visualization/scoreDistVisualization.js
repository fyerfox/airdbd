import React, { Component } from "react";
import * as d3 from "d3";
import mapData from "./worldMapData.geo.json";

// D3 visualization component
// Child of VisualizationPage component

// World heatmap of city scores
// Cities plotted on a world map with size/color
// representing their score

class ScoreDistVisualization extends Component {

    componentDidMount () {
        this.doApiRequest();
    }

    doApiRequest () {
        let url = "https://www.airdbd.net/api/cities/?limit=4500"
        fetch (url)
            .then (res => res.json())
            .then (data => this.appendData (data));
    }

    appendData (data) {
        var scores = data.results.map (instance => instance.score);
        scores = scores.filter(d => d != 0)
        this.drawHistogram (scores);
    }

    drawHistogram (scores) {
        // Find svg element
        var canvas = d3.select("body").select("#scoreDist");

        // draw blue background
        canvas.append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("height", 450)
            .attr("width", 660)
            .attr("fill", "white");

        // Sort scores into bins
        var bins = d3.histogram()
            .thresholds(14)
            (scores)

        // Make scales
        var xScale = d3.scaleLinear()
            .domain([0, Math.ceil(d3.max(scores))])
            .range([60, 650])

        var yScale = d3.scaleLinear()
            .domain([0, d3.max(bins.map(d => d.length)) + 30])
            .range([400, 0])

        var colorScale = d3.scaleLinear()
            .domain([0, 14])
            .range(["steelblue", "red"])

        // Create a group for the bars and bind data
        var bars = canvas.append ("g")
            .selectAll ("rect")
            .data (bins)
            .enter()

        // make a rectangle for each bin
        bars.append("rect")
            .attr("x", (d, i) => xScale((i + 1) / 2))
            .attr("y", d => yScale(d.length)) 
            .attr("height", d => yScale(0) - yScale(d.length))
            .attr("width", 35)
            .attr("fill", (d, i) => colorScale(i))

        // add text to each bar
        bars.append("text")
            .attr("x", (d, i) => xScale((i + 1) / 2) + 5)
            .attr("y", d => yScale(d.length) - 3)
            .attr("text-anchor", "left")
            .attr("fill", "black")
            .text(d => d.length)

        // Draw x and y scales
        var xAxis = d3.axisBottom()
            .scale(xScale)
            .ticks(14)

        var yAxis = d3.axisLeft()
            .scale(yScale)

        canvas.append("g")
            .attr ("transform", "translate(0, 400)")
            .call (xAxis);

        canvas.append("g")
            .attr ("transform", "translate(60, 0)")
            .call (yAxis);

        // Text labels for axes
        canvas.append("text")
            .attr("x", 320)
            .attr("y", 440)
            .text("Noise Score")

        canvas.append("text")
            .attr("x", 10) 
            .attr("y", 220)
            .text("#")


    }

    render() {
        return (
            <div>
                <svg
                  id="scoreDist"
                  height="450"
                  width="660" >
                    <text
                      x="280"
                      y="220"
                      textLength="100">
                        Loading... 
                    </text>
                </svg>
            </div>
        )
    }
}

export default ScoreDistVisualization;

