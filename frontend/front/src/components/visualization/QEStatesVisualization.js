import React, { Component } from "react";
import * as d3 from "d3";

import mapData from "./usMapData.geo.json"

// D3 visualization component
// Child of VisualizationPage component

// Quittin Emittin visualization.
// Map of each state showing emissions and laws per capita

class QEStatesVisualization extends Component {

    componentDidMount () {
        this.doApiRequest();
    }

    doApiRequest () {
        let url = "http://api.quittinemittin.me/api/v1/states?page=1&resultsPerPage=100"
        fetch (url)
            .then (res => res.json())
            .then (data => this.appendData (data));
    }

    appendData (data) {
        data = data.data
        this.draw (data);
    }

    draw (data) {
        // Find svg element
        var canvas = d3.select("body").select("#QEStates");

        // draw blue background
        canvas.append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("height", 450)
            .attr("width", 660)
            .attr("fill", "steelblue");

        // Path generator for continental states
        var continentalPathGen = d3.geoPath().projection(d3.geoOrthographic()
            .rotate([95, -37, 0])
            .scale(750)
            .translate([380, 200]));

        // Path generator for Alaska
        var alaskaPathGen = d3.geoPath().projection(d3.geoOrthographic()
            .rotate([149, -64, 0])
            .scale(400)
            .translate([250, 360]));

        // Path generator for Hawaii
        var hawaiiPathGen = d3.geoPath().projection(d3.geoOrthographic()
            .rotate([155, -19, 0])
            .scale(900)
            .translate([130, 350]));

        // Join geoJSON data with data from QE API
        var states = mapData.features.map (geoState => {
            return {
                geo: geoState,
                qe: data.filter (qeState => qeState.state_name === geoState.properties.NAME)[0],
                name: geoState.properties.NAME,
                pathGen:
                    (geoState.properties.NAME === "Alaska" ?
                        alaskaPathGen :
                    (geoState.properties.NAME === "Hawaii" ?
                        hawaiiPathGen :
                        continentalPathGen ))
            }
        }).filter (d => d.qe)

        // Create 2d color scale
        var emissionsPerCapita = data.map (d => (d.emissions / d.population))
        var laws = data.map (d => d.laws_passed)

        var emissionsColorScale = d3.scaleLinear()
            .domain([d3.min(emissionsPerCapita), d3.max(emissionsPerCapita)])
            .range([180, 50])

        var lawsColorScale = d3.scaleLinear()
            .domain([d3.min(laws), d3.max(laws)])
            .range([0, 1])

        var colorScale = (emissionsPerCapita, laws) => {
            var darkness = emissionsColorScale(emissionsPerCapita)
            var saturation = lawsColorScale(laws)
            var r = darkness + (saturation * (255 - darkness))
            var g = darkness
            var b = r
            return "rgb(" + r + "," + g + "," + b + ")"
        }


        // Create a new group ("g" element) for every
        // state in mapData.features
        var stateGroups = canvas.selectAll("g")
            .data (states)
            .enter()
            .append ("g");

        // Create a new path in each group for each state group
        var areas = stateGroups.append ("path")
            .attr("d", d => d.pathGen (d.geo))
            .attr("fill", d => colorScale(d.qe.emissions / d.qe.population, d.qe.laws_passed));

        console.log (states[0])
        console.log (states[0].pathGen.centroid(states[0].geo))

        // Create text showing number of laws
        stateGroups.append ("text")
            .attr("x", d => d.pathGen.centroid(d.geo)[0])
            .attr("y", d => d.pathGen.centroid(d.geo)[1])
            .attr("text-anchor", "middle")
            .attr("font-size", 10)
            .text(d => d.qe.laws_passed)

    }

    render() {
        return (
            <div>
                <svg
                  id="QEStates"
                  height="450"
                  width="660" >
                    <text
                      x="280"
                      y="220"
                      textLength="100">
                        Loading... 
                    </text>
                </svg>
            </div>
        )
    }
}

export default QEStatesVisualization;

