import React, { Component } from "react";
import * as d3 from "d3";

// D3 visualization component
// Child of VisualizationPage component

// Quittin Emittin visualization.
// Graph of number of laws over time. 

class QELawsVisualization extends Component {

    componentDidMount () {
        this.doApiRequest();
    }

    doApiRequest () {
        let url = "http://api.quittinemittin.me/api/v1/laws?page=1&resultsPerPage=50000"
        fetch (url)
            .then (res => res.json())
            .then (data => this.appendData (data));
    }

    appendData (data) {
        data = data.data  // this is the single worst line of code I have ever written
        this.draw (data);
    }

    draw (data) {
        // Find svg element
        var canvas = d3.select("body").select("#QELaws");

        // Draw a white background
        canvas.append ("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("width", 660)
            .attr("height", 450)
            .attr("fill", "white")

        // Extract relevant data
        var yearData = data.map(i => i.year)
        var yearFrequencies = d3.nest()
            .key (d => d)
            .entries (yearData)
            .map (d => {
                return {
                    key: d.key,
                    frequency: d.values.length
                }
            })
            .sort((a, b) => b.key - a.key)

        // Create time scale
        var xScale = d3.scaleLinear()
            .domain([d3.min(yearData) - 0.3, d3.max(yearData)])
            .range([80, 620])

        // Create frequency scale
        var yScale = d3.scaleLinear()
            .domain([0, 140])
            .range([400, 10])

        // Draw x and y scales
        var xAxis = d3.axisBottom()
            .scale(xScale)
            .ticks(7)

        var yAxis = d3.axisLeft()
            .scale(yScale)

        canvas.append("g")
            .attr ("transform", "translate(0, 400)")
            .call (xAxis);

        canvas.append("g")
            .attr ("transform", "translate(80, 0)")
            .call (yAxis);

        // Text labels for axes
        canvas.append("text")
            .attr("x", 320)
            .attr("y", 440)
            .text("Year")

        canvas.append("text")
            .attr("x", 10) 
            .attr("y", 220)
            .text("Laws")

        // draw line
        var path = d3.line()
            .x(d => xScale(d.key))
            .y(d => yScale(d.frequency))
            .curve(d3.curveLinear)
            (yearFrequencies)

        canvas.append("path")
            .attr("d", path)
            .attr("stroke", "red")
            .attr("stroke-width", 3)
            .attr("fill", "none")

        // Draw numbers above each point
        canvas.append("g")
            .selectAll("text")
            .data (yearFrequencies)
            .enter()
                .append("text")
                .attr("x", d => xScale(d.key)
                    - (d.key === "2018" ? 10 : 0)) // shift 2018's number to the left because the line is steep
                .attr("y", d => yScale(d.frequency) - 10)
                .attr("text-anchor", "middle")
                .attr("fill", "red")
                .text(d => d.frequency)

    }

    render() {
        return (
            <div>
                <svg
                  id="QELaws"
                  height="450"
                  width="660" >
                    <text
                      x="280"
                      y="220"
                      textLength="100">
                        Loading... 
                    </text>
                </svg>
            </div>
        )
    }
}

export default QELawsVisualization;

