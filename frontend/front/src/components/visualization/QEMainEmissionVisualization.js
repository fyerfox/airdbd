import React, { Component } from "react";
import * as d3 from "d3";

// D3 visualization component
// Child of VisualizationPage component

// Quittin Emittin visualization.
// Histogram of how many states have
// each emission type as their main emission type.


class QEMainEmissionVisualization extends Component {

    componentDidMount () {
        this.doApiRequest();
    }

    doApiRequest () {
        let url = "http://api.quittinemittin.me/api/v1/states?page=1&resultsPerPage=100"
        fetch (url)
            .then (res => res.json())
            .then (data => this.appendData (data));
    }

    appendData (data) {
        data = data.data
        this.draw (data);
    }

    draw (data) {
        // Find svg element
        var canvas = d3.select("body").select("#QEMainEmission");

        // draw white background
        canvas.append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("height", 450)
            .attr("width", 660)
            .attr("fill", "white");

        // extract largest contributors from the data, get frequencies
        var largestContributors = data.map (d => d.largest_contributor)
        var frequencies = d3.nest()
            .key (d => d)
            .entries (largestContributors)
            .map (d => {
                return {
                    key: d.key,
                    frequency: d.values.length
                }
            }).sort((a, b) => b.frequency - a.frequency)

        console.log (frequencies)

        // X and Y scales
        var xScale = d3.scaleLinear()
            .domain ([0, frequencies.length])
            .range ([80, 620])

        var yScale = d3.scaleLinear()
            .domain ([0, d3.max(frequencies.map (d => d.frequency))])
            .range ([410, 10])

        // Assign colors to each emmission type
        var colorScale = d3.scaleOrdinal()
            .domain ([
                "Coal",
                "Petroleum",
                "Electricity",
                "Natural Gas",
                "Industrial"
            ])
            .range ([
                "black",
                "brown",
                "yellow",
                "green",
                "gray"
            ])

        // Draw y scale
        var yAxis = d3.axisLeft()
            .scale(yScale)

        canvas.append("g")
            .attr ("transform", "translate(60, 0)")
            .call (yAxis);

        // Text labels for axes
        canvas.append("text")
            .attr("x", 320)
            .attr("y", 440)
            .text("Emission Type")

        canvas.append("text")
            .attr("x", 10)
            .attr("y", 220)
            .text("#")

        // Draw a bar for each emission type
        var bars = canvas.append ("g")
            .selectAll ("rect")
            .data(frequencies)
            .enter()

        bars.append ("rect")
            .attr ("x", (d, i) => xScale(i))
            .attr ("y", d => yScale(d.frequency))
            .attr ("height", d => yScale(0) - yScale(d.frequency))
            .attr ("width", 100)
            .attr ("fill", d => colorScale(d.key))

        bars.append ("text")
            .attr ("x", (d, i) => xScale(i) + 50)
            .attr ("y", d => (yScale(d.frequency) + 
                (d.key === "Industrial" ? -5 : 20)))
            .attr ("fill", d => (d.key === "Industrial" ? "black" : "white"))
            .attr ("text-anchor", "middle")
            .attr ("font-weight", "bold")
            .text (d => d.key)

    }

    render() {
        return (
            <div>
                <svg
                  id="QEMainEmission"
                  height="450"
                  width="660" >
                    <text
                      x="280"
                      y="220"
                      textLength="100">
                        Loading... 
                    </text>
                </svg>
            </div>
        )
    }
}

export default QEMainEmissionVisualization;

