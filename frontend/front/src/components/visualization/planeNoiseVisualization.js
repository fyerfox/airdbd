import React, { Component } from "react";
import * as d3 from "d3";
import mapData from "./worldMapData.geo.json";

// D3 visualization component
// Child of VisualizationPage component

// World heatmap of city scores
// Cities plotted on a world map with size/color
// representing their score

class PlaneNoiseVisualization extends Component {

    componentDidMount () {
        this.doApiRequest();
    }

    doApiRequest () {
        let url = "https://www.airdbd.net/api/aircrafttype/?limit=400"
        fetch (url)
            .then (res => res.json())
            .then (data => this.appendData (data));
    }

    appendData (data) {
        var airplaneData = data.results
            .map (instance => {
                return {
                    num:   instance.num_aircraft,
                    noise: instance.noise_level,
                    name:  instance.production_name
                }})
            .filter( i => (i.num != 0))

        this.drawPlot (airplaneData);
    }

    drawPlot (airplaneData) {
        // Find svg element
        var canvas = d3.select("body").select("#planeNoise");

        // draw blue background
        canvas.append("rect")
            .attr("x", 0)
            .attr("y", 0)
            .attr("height", 450)
            .attr("width", 660)
            .attr("fill", "white");

        var noiseData = airplaneData.map(i => i.noise)        
        var numData   = airplaneData.map(i => i.num)

        // Make scales
        var xScale = d3.scaleLinear()
            .domain([d3.min(noiseData) - 10, d3.max(noiseData) + 10])
            .range([60, 640])

        var yScale = d3.scaleLinear()
            .domain([d3.min(numData) - 10, d3.max(numData) + 100])
            .range([400, 30])

        var colorScale = d3.scaleLinear()
            .domain([d3.min(noiseData), 285, d3.max(noiseData)])
            .range(["green", "yellow", "red"])

        var sizeScale = d3.scaleLinear()
            .domain([d3.min(numData), d3.max(numData)])
            .range([2, 8])

        // Make axes
        var xAxis = d3.axisBottom()
            .scale(xScale)

        var yAxis = d3.axisLeft()
            .scale(yScale)

        canvas.append("g")
            .attr ("transform", "translate(0, 400)")
            .call (xAxis);

        canvas.append("g")
            .attr ("transform", "translate(60, 0)")
            .call (yAxis);

        // Make a group for the dots and bind the data
        var dotsGroup = canvas.append("g")
            .selectAll("circle")
            .data(airplaneData)
            .enter()

        // Make a dot for each data point
        dotsGroup.append("circle")
            .attr("cx", d => xScale(d.noise))
            .attr("cy", d => yScale(d.num))
            .attr("r", d => sizeScale(d.num))
            .attr("fill", d => colorScale(d.noise))

        // Put airplane name next to each dot
        dotsGroup.append("text")
            .attr("x", d => xScale(d.noise) + sizeScale(d.num) + 3)
            .attr("y", d => yScale(d.num) + 3)
            .attr("fill", "red")
            .attr("font-size", d => sizeScale(d.num) + 8)
            .text(d => {
                // Determines which points have names next to them.
                // Purely aesthetic, chosen through trial and error based
                // on the shape of the scatterplot
                if ((d.num + 3 * d.noise > 1420) && (d.name != "Boeing 757"))
                    return d.name
                return ""
            })

        // Text labels for axes
        canvas.append("text")
            .attr("x", 320)
            .attr("y", 440)
            .text("Noise Level")

        canvas.append("text")
            .attr("x", 10)
            .attr("y", 220)
            .text("#")

    }

    render() {
        return (
            <div>
                <svg
                  id="planeNoise"
                  height="450"
                  width="660" >
                    <text
                      x="280"
                      y="220"
                      textLength="100">
                        Loading... 
                    </text>
                </svg>
            </div>
        )
    }
}

export default PlaneNoiseVisualization;

