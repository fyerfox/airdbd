import React, { Component } from "react";
import { Banner } from "../";
import "./about.css";
import Danny from "../../pics/Member_Danny.jpg";
import Annan from "../../pics/Member_Annan.jpg";
import Vivian from "../../pics/Member_Vivian.jpg";
import Silvana from "../../pics/Member_Silvana.jpg";
//const fetch = require("node-fetch");
// Contents of the about page.
// Has a banner and all the information of the
// about page.
// Child of App component, hot-swapped during navigation.

class aboutPage extends Component {
  constructor(props) {
    super(props);
    // this.gitlabCommitsEndpoint =
    //   "https://gitlab.com/api/v4/projects/14580974/repository/contributors";
    this.gitlabCommitsEndpoint =
      "https://gitlab.com/api/v4/projects/14580974/repository/commits?per_page=287&all=true";
    this.gitlabIssuesEndpoint =
      "https://gitlab.com/api/v4/projects/14580974/issues?state=closed&per_page=100";
    this.state = {
      teamInfo: [
        {
          image: Danny,
          name: "Danny Tran",
          bio:
            "Junior CS student at UT Austin. Likes coding, gaming, and has a black cat named Phantom.",
          role: "Backend",
          commits: 0,
          issues: 0,
          unitTests: 20
        },
        {
          image: Annan,
          name: "Annan Wang",
          bio:
            "Junior CS student at UT Austin. Loves coding, gaming, traveling and photography.",
          role: "Frontend",
          commits: 0,
          issues: 0,
          unitTests: 36
        },
        {
          image: Vivian,
          name: "Vivian Cox",
          bio:
            "Senior CS student at UT Austin. Enjoys music andcats. Owns a skateboard for the aesthetic.",
          role: "Frontend",
          commits: 0,
          issues: 0,
          unitTests: 0
        },
        {
          image: Silvana,
          name: "Silvana Borgo",
          bio:
            "Junior CS student at UT Austin. Born and raised in Mexico. Loves playing basketball.",
          role: "Frontend",
          commits: 0,
          issues: 0,
          unitTests: 20
        }
      ]
    };
  }

  displayCard() {
    let out = [];
    for (const member of this.state.teamInfo) {
      out.push(
        <div className="card shadow">
          <img src={member.image} className="card-img-top" />
          <div className="card-body">
            <h3 className="card-title">{member.name}</h3>
            <p className="card-text">{member.bio}</p>
          </div>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">{member.role}</li>
            <li className="list-group-item">Commits: {member.commits}</li>
            <li className="list-group-item">Issues: {member.issues}</li>
            <li className="list-group-item">Unit Tests: {member.unitTests}</li>
          </ul>
        </div>
      );
    }
    return out;
  }

  componentWillMount() {
    fetch(this.gitlabCommitsEndpoint)
      .then(res => res.json())
      .then(data => this.writeCommit(data));
    fetch(this.gitlabIssuesEndpoint)
      .then(res => res.json())
      .then(data => this.writeIssue(data));
  }

  // writeCommit(data) {
  //   let stateCopy = this.state.teamInfo;
  //   let count = {};
  //   console.log(data);
  //   for (const member of stateCopy) {
  //     count[member.name] = 0;
  //   }
  //   for (const block of data) {
  //     count[block["name"]] += block["commits"];
  //   }
  //   for (const member of stateCopy) {
  //     member.commits = count[member.name];
  //   }
  //   this.setState({ teamInfo: stateCopy });
  // }
  // When using repo/commits
  writeCommit(data) {
    let stateCopy = this.state.teamInfo;
    let count = {};
    for (const member of stateCopy) {
      count[member.name] = 0;
    }
    for (const block of data) {
      count[block["committer_name"]] += 1;
    }
    for (const member of stateCopy) {
      member.commits = count[member.name];
    }
    this.setState({ teamInfo: stateCopy });
  }

  writeIssue(data) {
    let stateCopy = this.state.teamInfo;
    let count = {};
    for (const member of stateCopy) {
      count[member.name] = 0;
    }
    for (const block of data) {
      count[block.closed_by.name] += 1;
    }
    for (const member of stateCopy) {
      member.issues = count[member.name];
    }
    this.setState({ teamInfo: stateCopy });
  }

  getTotalCommits() {
    let count = 0;
    for (const member of this.state.teamInfo) {
      count += member.commits;
    }
    return count;
  }

  getTotalIssues() {
    let count = 0;
    for (const member of this.state.teamInfo) {
      count += member.issues;
    }
    return count;
  }

  getTotalTests() {
    let count = 0;
    for (const member of this.state.teamInfo) {
      count += member.unitTests;
    }
    return count;
  }

  render() {
    return (
      <React.Fragment>
          
        <Banner title="About Us" />
        <div className="container">
          {/* Banner Component */}
          {/* Content Here */}
          {/* <!-- Site Summary/Description --> */}
          <div className="container-fluid">
            <p>
              Don't like to hear noisy airplanes flying over your head? We don't
              either. To help you find a nice, quiet place to live, we gather air
              traffic data to determine where you will be the least bothered by
              air traffic. For any location in the continental United States, we
              assign it a "noise score" from 1 to 10 based on several factors,
              such as:
            </p>
            <ul className="list-unstyled">
              <li>How often planes pass overhead</li>
              <li>The altitude and engine loudness ratings of those planes</li>
              <li>Proximity to airports</li>
            </ul>
          </div>

          <h2>The Developers</h2>
          <div className="card-deck">{this.displayCard()}</div>

          {/* <!-- Statistics --> */}
          <div className="container-fluid">
            <h2>Statistics</h2>
            <div className="container">
              <div className="row">
                <div className="col-sm-4">
                  <div className="container-fluid bg-blue rounded shadow">
                    <h2>Total Commits:</h2>
                    <h3 id="total_commits">{this.getTotalCommits()}</h3>
                  </div>
                </div>
                <div className="col-sm-4">
                  <div className="container-fluid bg-blue rounded shadow">
                    <h2>Total Issues:</h2>
                    <h3 id="total_issues">{this.getTotalIssues()}</h3>
                  </div>
                </div>
                <div className="col-sm-4">
                  <div className="container-fluid bg-blue rounded shadow">
                    <h2>Total Unit Tests:</h2>
                    <h3 id="total_unittests">{this.getTotalTests()}</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* <!-- Data Sources --> */}
          <div className="container-fluid bg-grey rounded shadow">
            <h2>Data Sources</h2>
            <div className="container">
              <div className="row">
                <div className="col-sm-4 text-left">
                  <h3>
                    <a
                      href="https://www.opensky-network.org/apidoc"
                      id="linkAPI1"
                    >
                      Open Sky Network
                    </a>
                  </h3>
                  <p>
                    <em>
                      A service that tracks airplane flights in real time,
                      including both flight information and exact 3-dimensional
                      coordinates of the airplanes.
                    </em>
                  </p>
                  <p>
                    Open Sky does not maintain data about past flights. It
                    provides only real-time information. We obtain data over a
                    timeframe by continuously monitoring Open Sky over a rolling
                    period of two weeks.
                  </p>
                  <p>
                    At any given moment, we are tracking many US flights currently
                    in progress by querying Open Sky at set intervals of time to
                    reconstruct the plane's path through the sky. By monitoring
                    many flights continuously like this, we can obtain the most
                    up-to-date data and generate accurate statistics.
                  </p>
                </div>
                <div className="col-sm-4 text-left">
                  <h3>
                    <a href="https://geodb-cities-api.wirefreethought.com/">
                      GeoDB Cities
                    </a>
                  </h3>
                  <p>
                    <em>
                      Information about cities, including state, population,
                      precise latitude and longitude, and more.
                    </em>
                  </p>
                  <p>
                    We obtained our initial list of cities and associated
                    information by scraping this database. This gives us our list
                    of cities to track. Periodically, we query GeoDB Cities on the
                    cities we're tracking and update our data, just to keep
                    current records of data that changes over time, such as
                    population.
                  </p>
                </div>
                <div className="col-sm-4 text-left">
                  <h3>
                    <a href="https://developer.here.com/documentation/geocoder/topics/what-is.html">
                      Here Geocoder
                    </a>
                  </h3>
                  <p>
                    <em>
                      API that can translate addresses into lat/long coordinates,
                      and vise versa.
                    </em>
                  </p>
                  <p>
                    We use this API to translate user-provided addresses into
                    coordinates, so we can dynamically generate a noise score
                    based on any location. We also use the Geocoder to convert
                    coordinates into an address for the purpose of validating that
                    the given location is within the continental United States.
                  </p>
                </div>
              </div>
            </div>
          </div>

          {/* <!-- Tools --> */}
          <div className="container-fluid">
            <h2>Tools</h2>
            <div className="container">
              <div className="row">
                <div className="col-sm-3">
                  <h3>Bootstrap</h3>
                  <p>
                    <em>A HTML/CSS framework for creating reponsive websites</em>
                  </p>
                </div>
                <div className="col-sm-3">
                  <h3>GitLab</h3>
                  <p>
                    <em>Remote git repository hosting service</em>
                  </p>
                </div>
                <div className="col-sm-3">
                  <h3>Postman</h3>
                  <p>
                    <em>Tool for designing RESTful APIs</em>
                  </p>
                </div>
                <div className="col-sm-3">
                  <h3>Amazon Web Services (AWS)</h3>
                  <p>
                    <em>Backend toolset for hosting online services</em>
                  </p>
                </div>
                <div className="col-sm-3">
                  <h3>React</h3>
                  <p>
                    <em>The JavaScript library for building user interfaces</em>
                  </p>
                </div>
                <div className="col-sm-3">
                  <h3>Mocha</h3>
                  <p>
                    <em>
                      JavaScript test framework for makeing frontend unit tests
                    </em>
                  </p>
                </div>
                <div className="col-sm-3">
                  <h3>Selenium</h3>
                  <p>
                    <em>
                      Testing framework for making web applications accptence
                      tests
                    </em>
                  </p>
                </div>
                <div className="col-sm-3">
                  <h3>Slack</h3>
                  <p>
                    <em>The communication tool for group work</em>
                  </p>
                </div>
              </div>
            </div>
          </div>

          {/* <!-- Links --> */}
          <div className="container-fluid">
            <h2>Links</h2>
            <p>
              <a href="https://gitlab.com/fyerfox/airdbd" id="linkRepo">
                Gitlab Repo
              </a>
            </p>

          <p>
            <a
              href="https://documenter.getpostman.com/view/4253534/SW7XZp6Q?version=latest"
              id="linkPostman"
            >
              Postman API
            </a>
          </p>
        </div>

          {/* <!-- Disclaimer --> */}
          <div className="container-fluid">
            <h3>Data Integrity</h3>
            <p>
              The data shown on this website is only as good as our data sources
              can provide. Given this and the continued in-development status of
              this site, the data we have is incomplete and therefore may not
              reflect the whole of reality.
            </p>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default aboutPage;
