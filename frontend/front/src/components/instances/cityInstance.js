import React, { Component } from "react";
import InstancePage from "./instance.js"

// import { Map as LeafletMap, TileLayer, Marker, Popup } from "react-leaflet";
// import "leaflet/dist/leaflet.css";
// import L from "leaflet";

// The content of a city instance page.
// Contains a banner, the title
// of the page, and all the data and images
// for the instance.
// Child of App component, hot-swapped during navigation.

class CityInstancePage extends InstancePage {

    constructor (props) {
        super (props)
	this.state.r = false
	this.state.pairs = [{key: "City Name", value: "-"},
			    {key: "Country", value: "-"},
			    {key: "Score", value: "-"},
			    {key: "Latitude", value: "-"},
			    {key: "Longitude", value: "-"}]
	this.state.instanceData = [[{title : "", width: 12, fontSize: 20}]]
	
	this.modelType = "cities"
	    
        this.appendData = (data) => {
            // Make (directly) mutable copies of the state
            let new_pairs = this.state.pairs;
    	    let new_airports_rows = this.state.instanceData.slice(0, 1);

    	    // Get row data (for attribute table)
    	    new_pairs[0].value = data.city_name || new_pairs[0].value; // City name
    	    new_pairs[1].value = data.country || new_pairs[1].value; // Country
    	    new_pairs[2].value = data.score
      	    ? data.score.toFixed(2)
      	    : new_pairs[2].value; // Noise score
    	    new_pairs[3].value =
      	    data.position && data.position.coordinates
        	? data.position.coordinates[1]
        	: new_pairs[3].value; // Latitude
    	    new_pairs[4].value =
      	    data.position && data.position.coordinates
        	? data.position.coordinates[0]
        	: new_pairs[4].value; // Longitude

    	    if (data.airports) {
      	        for (var airport of data.airports) {
        	    if (airport.id && airport.airport_name)
          	        new_airports_rows.push([
            	        "/airport/" + airport.id,
            	        airport.airport_name
          		]);
      		}
    	    }

    	    // Get image source
    	    var new_image_src = data.images ? JSON.parse(data.images.replace(/'/g, "\""))[0] : "X";

    	    // Write the new state to this component
    	    this.setState({
      		image_src: new_image_src,
      		pageTitle: data.city_name,
      		pairs: new_pairs,
      		instanceData: new_airports_rows,
    	    });

	}
    }
}

export default CityInstancePage;
