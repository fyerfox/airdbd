import React, { Component } from "react";
import InstancePage from "./instance.js"

// The content of an airplane instance page.
// Contains a banner, the title
// of the page, and all the data and images
// for the instance.
// Child of App component, hot-swapped during navigation.

class AirplaneInstancePage extends InstancePage {

    constructor (props) {
	super (props)
	
	this.state.pairs = [{ key: "Airplane Type",    value: "-" },
            		    { key: "IATA Type",        value: "-" },
            		    { key: "Model Code",       value: "-" },
            		    { key: "Engine Count",     value: "-" },
            		    { key: "Engine Type",      value: "-" }]
	this.modelType = "aircraft"

  	this.appendData = (data) => {
    	    // Make (directly) mutable copies of the state
    	    let new_pairs         = this.state.pairs
	    
    	    // Get row data (for attribute table)
		
    	    new_pairs[0].value = data.aircraft_type && data.aircraft_type.production_name ?
        	data.aircraft_type.production_name : new_pairs[0].value
    	    new_pairs[1].value = data.IATA_type       || new_pairs[1].value
    	    new_pairs[2].value = data.model_code      || new_pairs[2].value
    	    new_pairs[3].value = data.engine_count    || new_pairs[3].value
    	    new_pairs[4].value = data.engine_type     || new_pairs[4].value

    	    // Set link address for aircraft type
    	    if (data.aircraft_type && data.aircraft_type.id)
        	new_pairs[0].link = "/airplanetype/" + data.aircraft_type.id

	
    	    // Write new state to this component
    	    this.setState({ image_src:     "X",
                    pageTitle:     "Airplane #" + (data.registration_number || "???"),
                    pairs:         new_pairs })
	}

    }
}

export default AirplaneInstancePage;
