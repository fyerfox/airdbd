import React, { Component } from "react";
import InstancePage from "./instance.js"

// import { Map as LeafletMap, TileLayer, Marker, Popup } from "react-leaflet";
// import "leaflet/dist/leaflet.css";
// import L from "leaflet";

// The content of an airport instance page.
// Contains a banner, the title
// of the page, and all the data and images
// for the instance.
// Child of App component, hot-swapped during navigation.

class AirportInstancePage extends InstancePage {

    constructor (props) {
	super (props)

	this.state.r = false
        this.state.pairs = [{ key: "Airport Name", value: "-" },
   			    { key: "ICAO Code", value: "-" },
      			    { key: "Country", value: "-" },
      			    { key: "Latitude", value: "-" },
      			    { key: "Longitude", value: "-" },
      			    { key: "Timezone", value: "-" }]

	this.modelType = "airports"
  	/* This function is called after an API fetch returns.
   	* The argument "data" is the json data returned by the fetch
   	* (converted to a JS object).
   	*/
  	this.appendData = (data) => {
    	    // Make (directly) mutable copies of the state
    	    let new_pairs = this.state.pairs;
    	    let new_rows_out = this.state.rows_out;
    	    let new_rows_in = this.state.rows_in;

    	    // Get row data (for attribute table)
    	    new_pairs[0].value = data.airport_name || new_pairs[0].value;
    	    new_pairs[1].value = data.icao_code || new_pairs[1].value;
    	    new_pairs[2].value = data.country || new_pairs[2].value;
    	    new_pairs[3].value =
      	    data.position && data.position.coordinates
        	? data.position.coordinates[1]
        	: new_pairs[3].value;
    	    new_pairs[4].value =
      	    data.position && data.position.coordinates
        	? data.position.coordinates[0]
        	: new_pairs[4].value;
    	    new_pairs[5].value = data.timezone || new_pairs[5].value;

    	    // Get image source
    	    let new_image_src = "X"; // TODO

    	    // Write the new state to this component
    	    this.setState({
      		image_src: new_image_src,
      		pageTitle: data.airport_name,
      		pairs: new_pairs,
    	    });
  	}

    }
}

export default AirportInstancePage;
