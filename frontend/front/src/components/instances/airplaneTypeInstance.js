import React, { Component } from "react";
import InstancePage from "./instance.js"

// The content of an airplane type instance page.
// Contains a banner, the title
// of the page, and all the data and images
// for the instance.
// Child of App component, hot-swapped during navigation.

class AirplaneTypeInstancePage extends InstancePage {

    constructor (props) {
	super (props)
	
	this.state.pairs = [{ key: "Production Name", value: "-" },
            		   { key: "IATA Code",       value: "-" },
            		   { key: "Noise Level",     value: "-", color: "red" },
            		   { key: "Number in Worldwide Fleet",     value: "-" },
            		   { key: "Percentage of Worldwide Fleet", value: "-" }]


	this.modelType = "aircrafttype"
  	
	this.appendData = (data) => {
    	    // Make (directly) mutable copies of the state
    	    let new_pairs         = this.state.pairs

    	    // Get row data (for attribute table)
    	    new_pairs[0].value = data.production_name || new_pairs[0].value
    	    new_pairs[1].value = data.iata_code       || new_pairs[1].value
    	    new_pairs[2].value = data.noise_level     || new_pairs[2].value
    	    new_pairs[3].value = data.num_aircraft    || new_pairs[3].value
    	    new_pairs[4].value = data.percentage ?
        	(data.percentage * 100).toFixed(5) + "%" : new_pairs[5].value

    	    // Get image source
            var new_image_src = data.images ? JSON.parse(data.images.replace(/'/g, "\""))[0] : "X";

    	    // Write new state to this component
    	    this.setState({ image_src:     new_image_src,
                    pageTitle:     data.production_name,
                    pairs:         new_pairs })
        }
    }
}

export default AirplaneTypeInstancePage;
