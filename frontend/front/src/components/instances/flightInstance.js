import React, { Component } from "react";
import InstancePage from "./instance.js"

// The content of a flight instance page.
// Contains a banner, the title
// of the page, and all the data and images
// for the instance.
// Child of App component, hot-swapped during navigation.

class FlightInstancePage extends InstancePage {

  constructor (props) {
    super (props)

    this.state.pairs = [{key: "Source Airport",	value: "-"},
	    		{key: "Destination Airport",	value: "-"},
	    		{key: "Airplane",		value: "-"},
	    		{key: "Airline IATA Number",	value: "-"},
	    		{key: "Status",		value: "-"}]

    this.modelType = "flights"
    
    /* This function is called after an API fetch returns.
     * The argument "data" is the json data returned by the fetch
     * (converted to a JS object).
     */
    this.appendData = (data) => {
      // Make (directly) mutable copies of the state
      let new_pairs         = this.state.pairs

      // Get row data (for attribute table)
      new_pairs[0].value = data.src && data.src.airport_name ?
          data.src.airport_name : new_pairs[0].value
      new_pairs[1].value = data.dst && data.dst.airport_name ?
          data.dst.airport_name : new_pairs[1].value
      new_pairs[2].value = data.aircraft ? data.aircraft.registration_number : new_pairs[2].value
      new_pairs[3].value = data.airline_iata_number || new_pairs[3].value
      new_pairs[4].value = data.status              || new_pairs[4].value

      // Set link addresses in attribute table
      // So airport and city entries are clickable, and link to that instance
      if (data.src && data.src.id)
          new_pairs[0].link = "/airport/" + data.src.id
      if (data.dst && data.dst.id)
          new_pairs[1].link = "/airport/" + data.dst.id
      if (data.aircraft && data.aircraft.id)
          new_pairs[2].link = "/airplane/" + data.aircraft.id

      // Get image source
      let new_image_src = "X"  // TODO

      // Write new state to this component
      this.setState({ image_src:     new_image_src,
                    pageTitle:     "Flight #" + (data.flight_icao_number || "???") ,
                    pairs:         new_pairs ,
      		    instanceData: [] })
    }
  }
}

export default FlightInstancePage;
