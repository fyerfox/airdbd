import React, {Component} from "react";
import Banner from "../banner/banner.js"
import InstanceTable from "../models/instanceTable.js"
import AttributeTable from "./attributeTable.js"

// Parent class for instance components
// Contains banner, instance table and 
// attribute table


class InstancePage extends Component {
    state = {
	    instanceData : [],
	    pairs: [],
	    pageTitle: "",
	    image_src: "X"
    };

    // properties for subclasses to define
    
    modelType = ""
    appendData = (data) => {};

    componentDidMount () {
        var api_url = "https://www.airdbd.net/api/" + this.modelType + "/" + this.props.match.params.instanceId + "/?format=json"
	fetch(api_url)
	    .then(res => res.json())
	    .then(data => this.appendData(data))
    }

    render() {
        return (
	    <div>
	       {/* Empty Banner */}
	       <Banner title="" subTitle="" img="X" />

		{/* Main Content of Page */}
		<div className="container">
		    <div className="row">
		        <div className="col-xs-12">
			    <h1>{this.state.pageTitle}</h1>
			</div>
		    </div>

		    {/* Attribute Table and Image */}
		    <div className="row">
			<div className="col-sm-6">
			    <AttributeTable pairs={this.state.pairs} />
			</div>
			<div className="col-sm-6">
			    <img src={this.state.image_src} width="400" />
			</div>
		    </div>

		    {this.state.instanceData.length > 0 &&
		        <div className="row">
			    <div className="col-sm-6 bg-grey">
			        <h5>
				    <strong> Airports </strong>
			        </h5>
			        <InstanceTable rows={this.state.instanceData} />
			    </div>
			</div>
		    }
		</div>
	    </div>
	);
    }

}

export default InstancePage;
