import React, { Component } from "react";
import { Link } from "react-router-dom";

// Table of attributes on an instance page.
// Has name-value pairs listed in a table.

// Data is passed down through the "pairs" attribute.
// e.g. <AttributeTable pairs={ js_array } />
// Where the js_array is an array of objects.
// Each object should have a key and value attribute.

// Example of an acceptable value:
// [ { key: "Name",   value: "John" },
//   { key: "Age",    value: 25 },
//   { key: "Gender", value: "Male" },
//   { key: "Favorite Color", value: "Male" },
//   { key: "Spouse", value: "Amy", link: "people/amy"}]

class AttributeTable extends Component {

  render() {

    // Build rows of table
    var tableRows = []
    var index = 0
    for (const pair of this.props.pairs) {
        var row =  (<div key={index} className={"row" + (index % 2 ? "" : " bg-grey")}>
                      <div className="col-sm-6">
                        <p style={{ fontSize: pair.fontSize || 15,
                                    color: pair.color || "black" }}>
                            <strong>{ pair.key }</strong>
                        </p>
                      </div>
                      <div className="col-sm-6">
                        <p style={{ fontSize: pair.fontSize || 15,
                                    color: pair.color || "black" }}>
                            { pair.value }
                        </p>
                      </div>
                    </div>)
        if (pair.link) {
            row = <Link to={pair.link}>{ row }</Link>
        }
        tableRows.push (row)
        index++
    }


    return (
        <div className="container-fluid">
            { tableRows }
        </div>
    );
  }
}

export default AttributeTable;
