import React from "react";
import { expect } from "chai";
import { shallow, mount, render } from "enzyme";
import Enzyme from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Navbar, Footer } from "./components";
import App from "./App.js";

Enzyme.configure({ adapter: new Adapter() });

describe("App.js", () => {
  it("has navbar", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.containsMatchingElement(<Navbar />)).to.equal(true);
  });
  it("has footer", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.containsMatchingElement(<Footer />)).to.equal(true);
  });
});
