from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest

class GUI_tests(unittest.TestCase):
    
    @classmethod
    def setUp(self):
        self.driver = webdriver.Chrome('/Users/silva/Downloads/chromedriver')
        self.url = "http://localhost:3000/"

    @classmethod
    def tearDown(self):
        self.driver.close()

    def test_home2(self):
        self.driver.get(self.url)
        self.driver.find_element_by_id('btnAirplane').click()
        self.assertEqual(self.driver.current_url, self.url + "airplanes")
        self.driver.back()
        self.driver.find_element_by_id('btnAirplaneType').click()
        self.assertEqual(self.driver.current_url, self.url + "airplanetypes")

    def test_navbar(self):
        self.driver.get(self.url)
        self.driver.find_element_by_link_text('PLANE TYPES').click()
        self.assertEqual(self.driver.current_url, self.url + "airplanetypes")
        self.driver.find_element_by_link_text('CITIES').click()
        self.assertEqual(self.driver.current_url, self.url + "cities")
        self.driver.find_element_by_link_text('ABOUT').click()
        self.assertEqual(self.driver.current_url, self.url + "about")

    def test_home(self):
        self.driver.get(self.url)
        self.driver.find_element_by_id('btnCity').click()
        self.assertEqual(self.driver.current_url, self.url + "cities")
        self.driver.back()
        self.driver.find_element_by_id('btnFlight').click()
        self.assertEqual(self.driver.current_url, self.url + "flights")
        self.driver.back()
        self.driver.find_element_by_id('btnAirport').click()
        self.assertEqual(self.driver.current_url, self.url + "airports")

    def test_cities(self):
        self.driver.get(self.url + "cities")
        cities = self.driver.find_elements_by_tag_name('a')[7].click()
        self.driver.back()
        self.assertEqual(self.driver.current_url, self.url + "cities")

    def test_flights(self):
        self.driver.get(self.url + "flights")
        self.driver.find_elements_by_tag_name('a')[7].click()
        self.driver.back()
        self.assertEqual(self.driver.current_url, self.url + "flights")

    def test_planeTypes(self):
        self.driver.get(self.url + "airplanetypes")
        self.driver.find_elements_by_tag_name('a')[7].click()
        self.driver.back()
        self.assertEqual(self.driver.current_url, self.url + "airplanetypes")

    def test_airplanes(self):
        self.driver.get(self.url + "airplanes")
        self.driver.find_elements_by_tag_name('a')[7].click()
        self.driver.back()
        self.assertEqual(self.driver.current_url, self.url + "airplanes")

    def test_about1(self):
        self.driver.get(self.url + "about/")
        self.driver.find_element_by_id('linkAPI1').click()
        self.assertEqual(self.driver.current_url, "https://opensky-network.org/apidoc/")

    def test_about2(self):
        self.driver.get(self.url + "about")
        self.driver.find_element_by_id('linkRepo').click()
        self.assertEqual(self.driver.current_url, "https://gitlab.com/fyerfox/airdbd")

    def test_about3(self):
        self.driver.get(self.url + "about")
        self.driver.find_element_by_id('linkPostman').click()
        self.assertEqual(self.driver.current_url, "https://documenter.getpostman.com/view/9026345/SVtR3WTD?version=latest")

    def test_globalSearch(self):
        self.driver.get(self.url)
        word = "Austin"
        searchBox = self.driver.find_element_by_id('searchBox')
        searchBox.send_keys(word)
        searchBox.submit()
        self.assertEqual(self.driver.current_url, self.url + "search/" + word)

    def test_wordSearch(self):
        self.driver.get(self.url)
        word = "Austin"
        searchBox = self.driver.find_element_by_id('searchBox')
        searchBox.send_keys(word)
        searchBox.submit()
        bannerTitle = self.driver.find_element_by_id('subtitle').text
        self.assertEqual(bannerTitle, "Search " + word)

if __name__ == "__main__":
    unittest.main()
