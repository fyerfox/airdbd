"""airdbd URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.http import HttpResponse
from rest_framework import routers

from airdbd.views import (
    AircraftViewSet,
    FlightViewSet,
    PointsViewSet,
    AircraftTypeViewSet,
    AirportViewset,
    NoiseScoreView,
    CityViewSet,
    Search,
)

from airdbd.tasks import (
    startScheduler,
    importAircraftTypes,
    importAirplanes,
    importAirports,
)

router = routers.DefaultRouter()
router.register(r"aircraft", AircraftViewSet)
router.register(r"flights", FlightViewSet)
router.register(r"points", PointsViewSet)
router.register(r"aircrafttype", AircraftTypeViewSet)
router.register(r"airports", AirportViewset)
router.register(r"cities", CityViewSet)

urlpatterns = [
    path("api/", include(router.urls)),
    path("api/score/", NoiseScoreView.as_view()),
    path("api/search/", Search.as_view()),
    path("api/admin/", admin.site.urls),
]
