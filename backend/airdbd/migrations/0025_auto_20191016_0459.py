# Generated by Django 2.2.5 on 2019-10-16 04:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('airdbd', '0024_auto_20191015_0913'),
    ]

    operations = [
        migrations.AlterField(
            model_name='flight',
            name='flight_icao_number',
            field=models.CharField(db_index=True, max_length=10),
        ),
    ]
