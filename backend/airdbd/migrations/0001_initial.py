# Generated by Django 2.2.5 on 2019-10-11 05:54

import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
from django.contrib.postgres.operations import BtreeGinExtension


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Aircraft',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('registration_number', models.CharField(max_length=6)),
                ('production_name', models.CharField(max_length=20)),
                ('IATA_type', models.CharField(max_length=20)),
                ('model_code', models.CharField(max_length=20)),
                ('engine_count', models.SmallIntegerField()),
                ('engine_type', models.CharField(max_length=10)),
                ('plane_class', django.contrib.postgres.fields.jsonb.JSONField()),
            ],
        ),
        BtreeGinExtension()
    ]
