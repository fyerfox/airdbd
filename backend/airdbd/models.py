from datetime import datetime

from django.contrib.postgres.fields import JSONField
from django.contrib.postgres.search import SearchVectorField
from django.contrib.postgres.indexes import GinIndex

from django.db import models

from django.contrib.gis.db import models as gismodels


class City(models.Model):
    position = gismodels.PointField()
    country = models.CharField(max_length=50)
    score = models.FloatField()
    city_name = models.CharField(max_length=50)
    search_document = SearchVectorField(null=True)
    images = models.TextField(null=True)

    class Meta:
        indexes = [GinIndex(fields=["search_document"])]


class Airport(models.Model):
    airport_name = models.CharField(max_length=100)
    icao_code = models.CharField(max_length=10)
    position = gismodels.PointField()
    country = models.CharField(max_length=50)
    timezone = models.CharField(max_length=30)
    city = models.ForeignKey(
        City, related_name="airports", on_delete=models.CASCADE, null=True
    )
    search_document = SearchVectorField(null=True)

    class Meta:
        indexes = [GinIndex(fields=["search_document"])]


class AircraftType(models.Model):
    production_name = models.CharField(max_length=100)
    iata_code = models.CharField(max_length=10, null=True)
    noise_level = models.FloatField(null=True)
    search_document = SearchVectorField(null=True)
    images = models.TextField(null=True)

    class Meta:
        indexes = [GinIndex(fields=["search_document"])]


class Aircraft(models.Model):
    registration_number = models.CharField(max_length=10, unique=True)
    aircraft_type = models.ForeignKey(
        AircraftType, related_name="aircraft", on_delete=models.CASCADE
    )
    IATA_type = models.CharField(max_length=20)
    model_code = models.CharField(max_length=20)
    engine_count = models.SmallIntegerField()
    engine_type = models.CharField(max_length=10)
    plane_class = JSONField(null=True)
    search_document = SearchVectorField(null=True)

    class Meta:
        indexes = [GinIndex(fields=["search_document"])]


class Flight(models.Model):
    flight_icao_number = models.CharField(max_length=10, db_index=True)
    src = models.ForeignKey(
        Airport, related_name="src_airport", on_delete=models.CASCADE, null=True
    )
    dst = models.ForeignKey(
        Airport, related_name="dst_airport", on_delete=models.CASCADE, null=True
    )
    status = models.CharField(max_length=15)
    airline_iata_number = models.CharField(max_length=5)
    aircraft = models.ForeignKey(
        Aircraft, related_name="flights", on_delete=models.CASCADE, null=True
    )
    search_document = SearchVectorField(null=True)

    class Meta:
        indexes = [GinIndex(fields=["search_document"])]


class FlightPoint(models.Model):
    flight = models.ForeignKey(Flight, related_name="points", on_delete=models.CASCADE)
    timestamp = models.DateTimeField()
    position = gismodels.PointField()
    altitude = models.FloatField()
    speed = models.FloatField()
    direction = models.FloatField()
