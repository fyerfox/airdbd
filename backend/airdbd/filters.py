import rest_framework_filters as filters
from airdbd.models import City, Airport, AircraftType, Aircraft, Flight


class CityFilter(filters.FilterSet):
    latitude = filters.NumberFilter(field_name="latitude")
    latitude__lt = filters.NumberFilter(field_name="latitude", lookup_expr="lt")
    latitude__gt = filters.NumberFilter(field_name="latitude", lookup_expr="gt")
    longitude = filters.NumberFilter(field_name="longitude")
    longitude__lt = filters.NumberFilter(field_name="longitude", lookup_expr="lt")
    longitude__gt = filters.NumberFilter(field_name="longitude", lookup_expr="gt")

    class Meta:
        model = City
        fields = {
            "country": ["exact"],
            "score": ["exact", "lt", "gt"],
            "city_name": ["exact"],
        }


class AirportFilter(filters.FilterSet):
    city = filters.RelatedFilter(
        CityFilter, field_name="city", queryset=City.objects.all()
    )
    latitude = filters.NumberFilter(field_name="latitude")
    latitude__lt = filters.NumberFilter(field_name="latitude", lookup_expr="lt")
    latitude__gt = filters.NumberFilter(field_name="latitude", lookup_expr="gt")
    longitude = filters.NumberFilter(field_name="longitude")
    longitude__lt = filters.NumberFilter(field_name="longitude", lookup_expr="lt")
    longitude__gt = filters.NumberFilter(field_name="longitude", lookup_expr="gt")

    class Meta:
        model = Airport
        fields = {
            "airport_name": ["exact"],
            "icao_code": ["exact"],
            "country": ["exact"],
            "timezone": ["exact"],
        }


class AircraftTypeFilter(filters.FilterSet):
    num_aircraft = filters.NumberFilter(field_name="num_aircraft")
    num_aircraft__lt = filters.NumberFilter(field_name="num_aircraft", lookup_expr="lt")
    num_aircraft__gt = filters.NumberFilter(field_name="num_aircraft", lookup_expr="gt")
    percentage = filters.NumberFilter(field_name="percentage")
    percentage__lt = filters.NumberFilter(field_name="percentage", lookup_expr="lt")
    percentage__gt = filters.NumberFilter(field_name="percentage", lookup_expr="gt")

    class Meta:
        model = AircraftType
        fields = {
            "production_name": ["exact"],
            "iata_code": ["exact"],
            "noise_level": ["exact", "lt", "gt"],
        }


class AircraftFilter(filters.FilterSet):
    aircraft_type = filters.RelatedFilter(
        AircraftTypeFilter,
        field_name="aircraft_type",
        queryset=AircraftType.objects.all(),
    )

    class Meta:
        model = Aircraft
        fields = {
            "registration_number": ["exact"],
            "IATA_type": ["exact"],
            "model_code": ["exact"],
            "engine_count": ["exact", "lt", "gt"],
            "engine_type": ["exact"],
        }


class FlightFilter(filters.FilterSet):
    src = filters.RelatedFilter(
        AirportFilter, field_name="src", queryset=Airport.objects.all()
    )
    dst = filters.RelatedFilter(
        AirportFilter, field_name="dst", queryset=Airport.objects.all()
    )

    class Meta:
        model = Flight
        fields = {
            "flight_icao_number": ["exact"],
            "status": ["exact"],
            "airline_iata_number": ["exact"],
        }
