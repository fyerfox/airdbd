from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from collections import OrderedDict


class SearchPaginator(PageNumberPagination):
    def get_paginated_response(self, data, time):
        return Response(
            OrderedDict(
                [
                    ("time", time),
                    (
                        "links",
                        {
                            "next": self.get_next_link(),
                            "previous": self.get_previous_link(),
                        },
                    ),
                    ("count", self.page.paginator.count),
                    ("results", data),
                ]
            )
        )
