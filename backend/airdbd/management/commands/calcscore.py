from django.core.management.base import BaseCommand, CommandError
from airdbd.models import City, FlightPoint
import time
import math

class Command(BaseCommand):
    help = 'Generate scores for all cities'

    def handle(self, *args, **options):
        total =  City.objects.count()
        print(total, 'Cities.')
        start = time.time()
        for i, c in enumerate(City.objects.all()):
            q = FlightPoint.objects.filter(position__distance_lte=(c.position, 8 * 1609.34))
            c.score = math.log(len(q) + 1, 3)
            c.save()
            print(i, '/', total, c.city_name, 'Averaging:', (time.time()-start) / (i + 1))