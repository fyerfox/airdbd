from django.core.management.base import BaseCommand, CommandError
from airdbd.models import Aircraft, AircraftType, City, Airport, AircraftType, Flight
from django.contrib.postgres.search import SearchVector
from django.db.models import Subquery, OuterRef

class Command(BaseCommand):
    help = 'Update indices of all models'
    
    def update_city(self):
        return City.objects.update(
            search_document=(
                SearchVector('country') +
                SearchVector('city_name', weight='A')
            )
        )

    def update_airport(self):
        city_name = City.objects.filter(
            id=OuterRef('city_id')
        ).values_list(
            'city_name'
        )

        return Airport.objects.update(
            search_document=(
                SearchVector('airport_name', weight='A') +
                SearchVector('icao_code') +
                SearchVector('country') +
                SearchVector('timezone') +
                SearchVector(Subquery(city_name))
            )
        )
    
    def update_aircraft_type(self):
        return AircraftType.objects.update(
            search_document=(
                SearchVector('production_name', weight='A') +
                SearchVector('iata_code')
            )
        )

    def update_aircraft(self):
        aircraft_name = AircraftType.objects.filter(
            id=OuterRef('aircraft_type_id')
        ).values_list(
            'production_name'
        )

        return Aircraft.objects.update(
            search_document=(
                SearchVector('registration_number', weight='A') +
                SearchVector(Subquery(aircraft_name)) +
                SearchVector('IATA_type') +
                SearchVector('model_code')
            )
        )
    
    def update_flight(self):
        aircraft_prod_name = AircraftType.objects.filter(
            id=OuterRef('aircraft')
        ).values_list(
            'production_name'
        )

        return Flight.objects.update(
            search_document=(
                SearchVector('flight_icao_number', weight='A') +
                SearchVector('status') +
                SearchVector('airline_iata_number') +
                SearchVector(Subquery(aircraft_prod_name))
            )
        )

    def handle(self, *args, **options):
        self.stdout.write(self.style.SUCCESS('Generating Indicies for Cities'))
        self.stdout.write(self.style.SUCCESS('{} entries updated.'.format(self.update_city())))
        self.stdout.write(self.style.SUCCESS('Generating Indicies for Airports'))
        self.stdout.write(self.style.SUCCESS('{} entries updated.'.format(self.update_airport())))
        self.stdout.write(self.style.SUCCESS('Generating Indicies for Aircraft Type'))
        self.stdout.write(self.style.SUCCESS('{} entries updated.'.format(self.update_aircraft_type())))
        self.stdout.write(self.style.SUCCESS('Generating Indicies for Aircraft'))
        self.stdout.write(self.style.SUCCESS('{} entries updated.'.format(self.update_aircraft())))
        self.stdout.write(self.style.SUCCESS('Generating Indicies for Flight'))
        self.stdout.write(self.style.SUCCESS('{} entries updated.'.format(self.update_flight())))
        