from django.core.management.base import BaseCommand, CommandError
from airdbd.models import City, AircraftType
import requests


class Command(BaseCommand):
    help = "Gather images for models using Wikipedia's API"

    def handle(self, *args, **options):
        for for num, aircraft in enumerate(AircraftType.objects.all()):
            print()
            search_query = aircraft.production_name

            url = "https://en.wikipedia.org/w/api.php?action=query&format=json&srsearch={}&list=search&gslimit=20&sroffset=0".format(
                search_query)

            # Get search results
            print('Searching:', search_query)

            search_results = requests.get(url).json()
            search_results = search_results['query']['search']

            if len(search_results) == 0:
                print('No results found. Skipping...')
                continue
            
            list(map(print, [(i, result['title']) for i, result in enumerate(search_results)]))

            index = len(search_results)
            while index is not 'n' and int(index) >= len(search_results):
                print("Pick one:")
                index = input()

            if  index is 'n' or int(index) < 0:
                print('Skipping...', search_query)
                continue

            index = int(index)
            url = "https://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=original&titles={}".format(
                search_results[index]['title'])

            result_image = requests.get(url).json()
            try:
                result_image = result_image['query']['pages'][str(search_results[index]['pageid'])]['original']['source']
            except KeyError:
                continue
            print("Link:", result_image)

            aircraft.images = [result_image]
            aircraft.save()

        return

        for num, city in enumerate(City.objects.filter(country='USA')):
            print()
            if city.images is not None:
                continue
            print(num, "of", City.objects.filter(country='USA').count())
            search_query = "{} United States {}".format(city.city_name, city.airports.all()[0].airport_name)

            url = "https://en.wikipedia.org/w/api.php?action=query&format=json&srsearch={}&list=search&gslimit=20&sroffset=0".format(
                search_query)

            # Get search results
            print('Searching:', search_query, 'in', city.country)
            print('Airports:')
            for airport in city.airports.all():
                print(airport.airport_name)
            
            search_results = requests.get(url).json()
            search_results = search_results['query']['search']

            if len(search_results) == 0:
                print('No results found. Skipping...')
                continue

            city_results = list(filter(lambda x: ',' in x[1]['title'], enumerate(search_results)))
            if len(city_results) == 1:
                index = city_results[0][0]
            elif ',' in search_results[0]['title'] or 'airport' in search_results[0]['title'].lower():
                index = 0
            else:
                list(map(print, [(i, result['title']) for i, result in enumerate(search_results)]))


                index = len(search_results)
                while index is not 'n' and int(index) >= len(search_results):
                    print("Pick one:")
                    index = input()

                if  index is 'n' or int(index) < 0:
                    print('Skipping...', search_query)
                    continue
            index = int(index)
            url = "https://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=original&titles={}".format(
                search_results[index]['title'])

            result_image = requests.get(url).json()
            try:
                result_image = result_image['query']['pages'][str(search_results[index]['pageid'])]['original']['source']
            except KeyError:
                continue
            print("Link:", result_image)

            city.images = [result_image]
            city.save()





