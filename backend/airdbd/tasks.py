from datetime import datetime, timedelta
from threading import Thread
from queue import Queue
import pytz
import requests
import json
import time

import schedule
import time
import threading

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist

from requests.exceptions import ConnectionError

from django.contrib.gis.geos import Point

from airdbd.models import Flight, FlightPoint, Aircraft, AircraftType, Airport


class Worker(Thread):
    """ Thread executing tasks from a given tasks queue """

    def __init__(self, tasks):
        Thread.__init__(self)
        self.tasks = tasks
        self.daemon = True
        self.start()

    def run(self):
        while True:
            func, args, kargs = self.tasks.get()
            try:
                func(*args, **kargs)
            except Exception as e:
                # An exception happened in this thread
                print(e)
            finally:
                # Mark this task as done, whether an exception happened or not
                self.tasks.task_done()


class ThreadPool:
    """ Pool of threads consuming tasks from a queue """

    def __init__(self, num_threads):
        self.tasks = Queue(num_threads)
        for _ in range(num_threads):
            Worker(self.tasks)

    def add_task(self, func, *args, **kargs):
        """ Add a task to the queue """
        self.tasks.put((func, args, kargs))

    def map(self, func, args_list):
        """ Add a list of tasks to the queue """
        for args in args_list:
            self.add_task(func, args)

    def wait_completion(self):
        """ Wait for completion of all the tasks in the queue """
        self.tasks.join()


pool = ThreadPool(50)


def importAirports():
    with open("airports", "r") as file:
        airports = json.load(file)
        for num, airport in enumerate(airports):
            if airport["codeIcaoAirport"] is None:
                continue
            if Airport.objects.filter(icao_code=airport["codeIcaoAirport"]).exists():
                continue
            airport, created = Airport.objects.get_or_create(
                airport_name=airport["nameAirport"],
                icao_code=airport["codeIcaoAirport"],
                position=Point(
                    float(airport["longitudeAirport"]),
                    float(airport["latitudeAirport"]),
                ),
                country=airport["nameCountry"],
                timezone=airport["timezone"],
            )

            if created:
                print("Created: ", airport.airport_name)
                pass

            print(num, len(airports))


def importAircraftTypes():
    with open("aircrafttypes", "r") as file:
        aircraft_types = json.load(file)
        for num, aircraft_type in enumerate(aircraft_types):
            if AircraftType.objects.filter(
                production_name=aircraft_type["nameAircraft"]
            ).exists():
                continue
            aircraft_type, created = AircraftType.objects.get_or_create(
                production_name=aircraft_type["nameAircraft"],
                iata_code=aircraft_type["codeIataAircraft"],
                noise_level=None,
            )

            if created:
                print("Created: ", aircraft_type.production_name)
                pass

            print(num, len(aircraft_types))


def getAircraftType(production_name):
    aircraft_type, created = AircraftType.objects.get_or_create(
        production_name=production_name
    )
    return aircraft_type


# Only used once to mass import airplanes from downloaded database
def importAirplanes():
    with open("planes", "r") as file:
        planes = json.load(file)
        for num, plane in enumerate(planes):
            if Aircraft.objects.filter(
                registration_number=plane["numberRegistration"]
            ).exists():
                continue
            aircraft, created = Aircraft.objects.get_or_create(
                registration_number=plane["numberRegistration"],
                aircraft_type=getAircraftType(plane["productionLine"]),
                IATA_type=plane["airplaneIataType"],
                model_code=plane["modelCode"],
                engine_count=plane["enginesCount"],
                engine_type=plane["enginesType"],
                plane_class=plane["planeClass"],
            )

            if created:
                print("Created: ", aircraft.registration_number)
                pass

            print(num, len(planes))


def fetchAircraftDetails(registration_number):
    aircraft = requests.get(
        "http://aviation-edge.com/v2/public/airplaneDatabase?key="
        + settings.AVIATION_EDGE_API_KEY
        + "&numberRegistration="
        + registration_number
    ).json()

    if "error" not in aircraft:
        return aircraft

    return None


def getAircraft(registration_number):
    try:
        aircraft = Aircraft.objects.get(registration_number=registration_number)
    except ObjectDoesNotExist:
        aircraft_res = fetchAircraftDetails(registration_number)
        if aircraft_res:
            aircraft = Aircraft.objects.create(
                registration_number=aircraft_res["numberRegistration"],
                aircraft_type=getAircraftType(aircraft_res["productionLine"]),
                IATA_type=aircraft_res["airplaneIataType"],
                model_code=aircraft_res["modelCode"],
                engine_count=aircraft_res["enginesCount"],
                engine_type=aircraft_res["enginesType"],
                plane_class=aircraft_res["planeClass"],
            )
        else:
            return None
    return aircraft


def getAirport(icao_code):
    try:
        airport = Airport.objects.get(icao_code=icao_code)
        return airport
    except ObjectDoesNotExist:
        return None


def processFlightPoint(flight):
    aircraft = getAircraft(flight["aircraft"]["regNumber"])

    try:
        f = Flight.objects.get(
            flight_icao_number=flight["flight"]["icaoNumber"], aircraft=aircraft
        )
    except ObjectDoesNotExist:
        if flight["status"] != "en-route":
            return
        f = Flight.objects.create(
            flight_icao_number=flight["flight"]["icaoNumber"],
            src=getAirport(flight["departure"]["icaoCode"]),
            dst=getAirport(flight["arrival"]["icaoCode"]),
            status=flight["status"],
            airline_iata_number=flight["airline"]["iataCode"],
            aircraft=aircraft,
        )

        # print("Created flight: ", f.flight_icao_number)
    if f.status != "en-route":
        return

    if flight["status"] is not "en-route" and f.status is "en-route":
        f.status = flight["status"]
        f.save()

    if flight["speed"]["horizontal"] == 0:
        f.status = "landed"
        f.save()

    fp = FlightPoint(
        flight=f,
        timestamp=datetime.fromtimestamp(float(flight["system"]["updated"]), pytz.UTC),
        position=Point(
            flight["geography"]["longitude"], flight["geography"]["latitude"]
        ),
        altitude=flight["geography"]["altitude"],
        speed=flight["speed"]["horizontal"],
        direction=flight["geography"]["direction"],
    )

    if not FlightPoint.objects.filter(
        timestamp=fp.timestamp, flight=fp.flight
    ).exists():
        print("Creating FP: ", fp.position)
        fp.save()
    else:
        # print("Not creating FP")
        pass


def getFlights():
    try:
        print("Starting getting flights")
        start = time.time()
        res = requests.get(
            "http://aviation-edge.com/v2/public/flights?key="
            + settings.AVIATION_EDGE_API_KEY
        )
        pool.map(processFlightPoint, res.json())
        pool.wait_completion()
        end = time.time()
        print(end - start)
        print(len(res.json()))
    except ConnectionError:
        print("connection error")
        pass


def scheduler():
    print("????")
    getFlights()
    schedule.every(5).minutes.do(getFlights)
    while True:
        schedule.run_pending()
        time.sleep(1)


def startScheduler():
    t = threading.Thread(target=scheduler, name="scheduler")
    t.daemon = True
    t.start()
