import json
import time
import re
import math

from collections import OrderedDict

from rest_framework import status
from rest_framework import viewsets, mixins
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from rest_framework.filters import SearchFilter

from django.core import serializers
from django.core.paginator import Paginator
from django.core.exceptions import FieldDoesNotExist, FieldError
from django.http import HttpResponse
from django.db.models import (
    Count,
    F,
    ExpressionWrapper,
    Sum,
    FloatField,
    CharField,
    Q,
    Func,
)
from django.db.models.functions import Cast, Greatest
from django.db import models

from django_filters import rest_framework as filters

from django.contrib.postgres.search import SearchQuery, SearchVector, SearchRank

from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance
from django.contrib.gis.db.models.functions import AsGeoJSON

from airdbd.models import Aircraft, Flight, FlightPoint, AircraftType, Airport, City
from airdbd.serializers import (
    AircraftSerializer,
    FlightSerializer,
    FlightPointSerializer,
    AircraftTypeSerializer,
    AirportSerializer,
    ScoreParameterSerializer,
    CitySerializer,
    AircraftTypeSummarySerializer,
    SearchResultSerializer,
    AircraftSummarySerializer,
    CitySummarySerializer,
    FlightSummarySerializer,
)
from airdbd.paginator import SearchPaginator
from airdbd.filters import (
    CityFilter,
    AirportFilter,
    AircraftTypeFilter,
    AircraftFilter,
    FlightFilter,
)


def handle_queryset_sort(order_by, queryset, class_type):
    if order_by:
        queryset = queryset.order_by(order_by)

    return queryset


class AirportViewset(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    queryset = Airport.objects.prefetch_related("city")
    serializer_class = AirportSerializer
    filterset_class = AirportFilter
    filter_backends = [SearchFilter, filters.DjangoFilterBackend]
    search_fields = ["airport_name", "icao_code", "country", "timezone"]

    def list(self, request):
        order_by = self.request.query_params.get("order_by", "")
        queryset = self.get_queryset().annotate(
            latitude=Cast(Func("position", function="ST_Y"), FloatField()),
            longitude=Cast(Func("position", function="ST_X"), FloatField()),
        )

        queryset = self.filter_queryset(queryset)

        queryset = handle_queryset_sort(order_by, queryset, Airport)
        if isinstance(queryset, Response):
            return queryset

        try:
            page = self.paginate_queryset(queryset)
        except FieldError:
            return Response(
                {"error": "invalid sort_by parameter"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class AircraftViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    queryset = Aircraft.objects.prefetch_related("aircraft_type")
    serializer_class = AircraftSerializer
    filterset_class = AircraftFilter
    filter_backends = [SearchFilter, filters.DjangoFilterBackend]
    search_fields = [
        "registration_number",
        "IATA_type",
        "model_code",
        "engine_count",
        "engine_type",
        "aircraft_type__production_name",
    ]

    def list(self, request):
        order_by = self.request.query_params.get("order_by", "")
        queryset = self.filter_queryset(self.get_queryset())

        queryset = handle_queryset_sort(order_by, queryset, Aircraft)
        if isinstance(queryset, Response):
            return queryset

        try:
            page = self.paginate_queryset(queryset)
        except FieldError:
            return Response(
                {"error": "invalid sort_by parameter"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class FlightViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    queryset = Flight.objects.prefetch_related(
        "src", "dst", "aircraft", "aircraft__aircraft_type", "src__city", "dst__city"
    ).order_by("src")
    serializer_class = FlightSerializer
    filterset_class = FlightFilter
    filter_backends = [SearchFilter, filters.DjangoFilterBackend]
    search_fields = ["flight_icao_number", "status", "airline_iata_number"]

    def list(self, request):
        order_by = self.request.query_params.get("order_by", "")
        queryset = self.filter_queryset(self.get_queryset())

        queryset = handle_queryset_sort(order_by, queryset, Flight)
        if isinstance(queryset, Response):
            return queryset

        try:
            page = self.paginate_queryset(queryset)
        except FieldError:
            return Response(
                {"error": "invalid sort_by parameter"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class PointsViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    queryset = FlightPoint.objects.all()
    serializer_class = FlightPointSerializer


class AircraftTypeViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    total_aircraft = Aircraft.objects.count()
    queryset = AircraftType.objects.annotate(
        num_aircraft=Count("aircraft"),
        percentage=Cast(Count("aircraft"), FloatField()) / total_aircraft,
    ).order_by("-num_aircraft")
    serializer_class = AircraftTypeSerializer
    filterset_class = AircraftTypeFilter
    filter_backends = [SearchFilter, filters.DjangoFilterBackend]
    search_fields = ["production_name", "iata_code", "noise_level"]

    def list(self, request):
        order_by = self.request.query_params.get("order_by", "")
        queryset = self.filter_queryset(self.get_queryset())

        queryset = handle_queryset_sort(order_by, queryset, AircraftType)
        if isinstance(queryset, Response):
            return queryset

        try:
            page = self.paginate_queryset(queryset)
        except FieldError:
            return Response(
                {"error": "invalid sort_by parameter"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class CityViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    queryset = City.objects.prefetch_related("airports")
    serializer_class = CitySerializer
    filterset_class = CityFilter
    filter_backends = [SearchFilter, filters.DjangoFilterBackend]
    search_fields = ["country", "score", "city_name"]

    def list(self, request):
        order_by = self.request.query_params.get("order_by", "")
        queryset = self.get_queryset().annotate(
            latitude=Cast(Func("position", function="ST_Y"), FloatField()),
            longitude=Cast(Func("position", function="ST_X"), FloatField()),
        )

        queryset = self.filter_queryset(queryset)

        queryset = handle_queryset_sort(order_by, queryset, City)
        if isinstance(queryset, Response):
            return queryset

        try:
            page = self.paginate_queryset(queryset)
        except FieldError:
            return Response(
                {"error": "invalid sort_by parameter"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class NoiseScoreView(APIView):
    def post(self, request):
        serializer = ScoreParameterSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        data = serializer.validated_data

        queryset = FlightPoint.objects.filter(
            position__distance_lte=(
                Point(data["longitude"], data["latitude"], srid=4326),
                8 * 1609.34,
            )
        )

        return Response({"score": math.log((len(queryset) + 1), 3)})


def load_mixed_objects(dicts):
    """
    Takes a list of dictionaries, each of which must at least have a 'type'
    and a 'pk' key. Returns a list of ORM objects of those various types.
    Each returned ORM object has a .original_dict attribute populated.
    """
    to_fetch = {}
    for d in dicts:
        to_fetch.setdefault(d["type"], set()).add(d["pk"])
    fetched = {}
    for key, model in (
        ("City", City),
        ("Airport", Airport),
        ("AircraftType", AircraftType),
        ("Aircraft", Aircraft),
        ("Flight", Flight),
    ):
        ids = to_fetch.get(key) or []
        objects = model.objects.filter(pk__in=ids)
        if key == "Aircraft":
            objects = objects.prefetch_related("aircraft_type")
        if key == "Flight":
            objects = objects.prefetch_related("aircraft", "aircraft__aircraft_type")

        for obj in objects:
            fetched[(key, obj.pk)] = obj
    # Build list in same order as dicts argument
    to_return = []
    for d in dicts:
        item = fetched.get((d["type"], d["pk"])) or None
        if isinstance(item, Aircraft):
            item = AircraftSummarySerializer(item).data
        if isinstance(item, City):
            item = CitySummarySerializer(item).data
        if isinstance(item, Airport):
            item = AirportSerializer(item).data
        if isinstance(item, AircraftType):
            item = AircraftTypeSummarySerializer(item).data
        if isinstance(item, Flight):
            item = FlightSummarySerializer(item).data
        if item:
            item.original_dict = d
            to_return.append(item)
    return to_return


class Search(APIView):
    def get(self, request):
        query = request.GET.get("search", "").strip()
        t = request.GET.get("type", "").strip()

        if query is "":
            return Response(
                {"error": "query cannot be empty"}, status=status.HTTP_400_BAD_REQUEST
            )

        # TODO: can easily extract this into a function
        types = {
            "City": (City,),
            "Airport": (Airport,),
            "AircraftType": (AircraftType,),
            "Aircraft": (Aircraft,),
            "Flight": (Flight,),
            "": (City, Airport, AircraftType, Aircraft, Flight),
        }

        try:
            type_to_search = types[t]
        except KeyError:
            return Response(
                {
                    "error": "optional type field must be City, Airport, AircraftType, Aircraft, or Flight"
                }
            )

        start = time.time()

        query_set = (
            Aircraft.objects.annotate(
                type=models.Value("empty", output_field=models.CharField()),
                rank=SearchRank(
                    SearchVector(Cast("search_document", CharField())), query
                ),
            )
            .values("pk", "type", "rank")
            .none()
        )

        for model in type_to_search:
            query_set = query_set.union(
                model.objects.annotate(
                    rank=SearchRank(
                        SearchVector(Cast("search_document", CharField())), query
                    ),
                    type=models.Value(model.__name__, output_field=models.CharField()),
                )
                .filter(rank__gt=0.0000001)
                .values("pk", "type", "rank")
            )

        query_set = query_set.order_by("-rank")

        paginator = SearchPaginator()
        page = paginator.paginate_queryset(query_set, request)

        results = []
        for obj in load_mixed_objects(page):
            results.append(
                OrderedDict(
                    [
                        ("type", obj.original_dict["type"]),
                        ("rank", obj.original_dict.get("rank")),
                        ("obj", obj),
                    ]
                )
            )

        end = time.time()

        results = paginator.get_paginated_response(
            results, "{0:.3f}".format(end - start)
        )
        return results
