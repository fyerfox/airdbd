from django.test import TestCase
from airdbd.models import City, Airport, AircraftType, Flight, FlightPoint, Aircraft
from airdbd.filters import CityFilter
from airdbd.views import CityViewSet
from django.contrib.gis.geos import Point
from django.core.exceptions import ObjectDoesNotExist
from django.db.utils import DataError
from rest_framework.test import APITestCase


class CityTestCase(TestCase):
    def setUp(self):
        City.objects.create(
            position=Point(40, 100), country="Neverland", score=1, city_name="Mocha"
        )

        City.objects.create(
            position=Point(139.839478, 35.652832),
            country="Japan",
            score=0.01,
            city_name="Tokyo",
        )

    def test_city_creation_fetch(self):
        city = City.objects.get(id=1)
        self.assertEqual(city.city_name, "Mocha")
        city2 = City.objects.get(id=2)
        self.assertEqual(city2.city_name, "Tokyo")

    def test_city_defaults(self):
        city = City()
        self.assertEqual(city.position, None)
        self.assertEqual(city.country, "")
        self.assertEqual(city.score, None)
        self.assertEqual(city.city_name, "")

    def test_city_does_not_exist(self):
        try:
            City.objects.get(city_name="Austin")
            self.fail("City should not have existed")
        except ObjectDoesNotExist:
            pass


class AirportTestCase(TestCase):
    def setUp(self):
        Airport.objects.create(
            airport_name="Houston",
            icao_code="IAH",
            position=Point(40, 100),
            country="US",
            city=None,
        )

    def test_airport_creation_fetch(self):
        airport = Airport.objects.get(id=1)
        self.assertEqual(airport.airport_name, "Houston")

    def test_field_limit(self):
        try:
            Airport.objects.create(airport_name="Bobby", icao_code="12345678910")
            self.fail("Airport icao field should not exceed 10")
        except DataError:
            pass

    def test_airport_does_not_exist(self):
        try:
            Airport.objects.get(airport_name="ATX")
            self.fail("Airport should not have existed")
        except ObjectDoesNotExist:
            pass


class AircraftTypeTestCase(TestCase):
    def setUp(self):
        AircraftType.objects.create(
            production_name="Boeing 747", iata_code="idk", noise_level="50"
        )

    def test_aircraft_type_creation_fetch(self):
        aircraft_type = AircraftType.objects.filter(production_name="Boeing 747")[0]
        self.assertEqual(aircraft_type.production_name, "Boeing 747")

    def test_field_limit(self):
        try:
            AircraftType.objects.create(iata_code="12345678910")
            self.fail("AircraftType iata field should not exceed 10")
        except DataError:
            pass

    def test_airport_type_does_not_exist(self):
        try:
            AircraftType.objects.get(production_name="Airbus A320")
            self.fail("AircraftType should not have existed")
        except ObjectDoesNotExist:
            pass


class AircraftTestCase(TestCase):
    def setUp(self):
        AircraftType.objects.create(
            production_name="Boeing 747", iata_code="idk", noise_level="50"
        )

        aircraft_type = AircraftType.objects.filter(production_name="Boeing 747")[0]

        Aircraft.objects.create(
            registration_number="900142069",
            aircraft_type=aircraft_type,
            IATA_type="747",
            model_code="Boeing 747",
            engine_count=4,
            engine_type="JET",
        )

    def test_aircraft_creation_fetch(self):
        aircraft = Aircraft.objects.get(id=1)
        self.assertEqual(aircraft.registration_number, "900142069")

    def test_field_limit(self):
        try:
            Aircraft.objects.create(registration_number="12345678910")
            self.fail("registration_number field should not exceed 10")
        except DataError:
            pass

    def test_aircraft_does_not_exist(self):
        try:
            Aircraft.objects.get(registration_number="9002")
            self.fail("Aircraft should not have existed")
        except ObjectDoesNotExist:
            pass


class FilterCityTestCase(APITestCase):
    def setUp(self):
        City.objects.create(
            position=Point(40, 100), country="Neverland", score=1, city_name="Mocha"
        )
        City.objects.create(
            position=Point(0, 0),
            country="The Atlantic Ocean",
            score=10,
            city_name="Atlantis",
        )
        City.objects.create(
            position=Point(113.9876182, 22.3529808),
            country="China",
            score=1,
            city_name="Hong Kong",
        )
        City.objects.create(
            position=Point(135.5787927, 35.0984403),
            country="Japan",
            score=3,
            city_name="Kyoto",
        )
        City.objects.create(
            position=Point(126.9195108, 37.5652154),
            country="South Korea",
            score=2,
            city_name="Seoul",
        )

    def test_city_count(self):
        self.assertEqual(City.objects.count(), 5)

    def test_city_1(self):
        response = self.client.get("/api/cities/?country=South Korea")
        self.assertEqual(response.data["count"], 1)

    def test_city_2(self):
        response = self.client.get("/api/cities/?score__lt=2")
        self.assertEqual(response.data["count"], 2)

    def test_city_3(self):
        response = self.client.get("/api/cities/?city_name=Austin")
        self.assertEqual(response.data["count"], 0)

    def test_city_4(self):
        response = self.client.get("/api/cities/?city_name=Kyoto")
        self.assertEqual(response.data["count"], 1)


class FilterAirportTestCase(APITestCase):
    def setUp(self):
        Airport.objects.create(
            airport_name="Houston",
            icao_code="IAH",
            position=Point(40, 100),
            country="US",
            city=None,
        )
        city = City.objects.create(
            position=Point(-97.7420657, 30.2741716),
            country="USA",
            score=2,
            city_name="Austin",
        )
        Airport.objects.create(
            airport_name="Austin",
            icao_code="IAH",
            position=Point(-97.7420657, 30.2741716),
            country="US",
            city=city,
        )

    def test_airport_count(self):
        self.assertEqual(Airport.objects.count(), 2)

    def test_airport_1(self):
        response = self.client.get("/api/airports/?airport_name=Austin")
        self.assertEqual(response.data["count"], 1)

    def test_airport_2(self):
        response = self.client.get("/api/airports/?latitude__lt=50")
        self.assertEqual(response.data["count"], 1)

    def test_airport_3(self):
        response = self.client.get("/api/airports/?latitude__gt=50")
        self.assertEqual(response.data["count"], 1)

    def test_airport_4(self):
        response = self.client.get("/api/airports/?airport_name=Tokyo")
        self.assertEqual(response.data["count"], 0)

    def test_airport_5(self):
        response = self.client.get("/api/airports/?city__city_name=Austin")
        self.assertEqual(response.data["count"], 1)


class FilterAircraftTypeTestCase(TestCase):
    def setUp(self):
        AircraftType.objects.create(
            production_name="Boeing 747", iata_code="idk", noise_level="50"
        )
        AircraftType.objects.create(
            production_name="Boeing 737", iata_code="idk", noise_level="70"
        )
        AircraftType.objects.create(
            production_name="SR-71", iata_code="idk", noise_level="20"
        )

    def test_aircraft_type_count(self):
        self.assertEqual(AircraftType.objects.count(), 3)

    def test_aircraft_type_1(self):
        response = self.client.get("/api/aircrafttype/?production_name=SR-71")
        self.assertEqual(response.data["count"], 1)

    def test_aircraft_type_2(self):
        response = self.client.get("/api/aircrafttype/?noise_level__lt=50")
        self.assertEqual(response.data["count"], 1)

    def test_aircraft_type_3(self):
        response = self.client.get("/api/aircrafttype/?noise_level__gt=50")
        self.assertEqual(response.data["count"], 1)

    def test_aircraft_type_4(self):
        response = self.client.get("/api/aircrafttype/?production_name=F-18")
        self.assertEqual(response.data["count"], 0)

    def test_aircraft_type_5(self):
        response = self.client.get("/api/aircrafttype/?percentage__lt=0.4")
        self.assertEqual(response.data["count"], 3)
