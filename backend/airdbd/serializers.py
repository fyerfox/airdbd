import random

from rest_framework import serializers
from rest_framework_gis.serializers import GeoModelSerializer

from airdbd.models import Aircraft, Flight, FlightPoint, AircraftType, Airport, City


class AirportSerializer(GeoModelSerializer):
    city_name = serializers.CharField(read_only=True, source="city.city_name")
    position = serializers.SerializerMethodField()

    def get_position(self, obj):
        return {"latitude": obj.position[1], "longitude": obj.position[0]}

    class Meta:
        model = Airport
        fields = [
            "id",
            "airport_name",
            "icao_code",
            "position",
            "country",
            "timezone",
            "city_name",
        ]


class AircraftTypeSummarySerializer(serializers.ModelSerializer):
    class Meta:
        model = AircraftType
        fields = ["id", "production_name", "iata_code", "noise_level"]


class AircraftTypeSerializer(serializers.ModelSerializer):
    num_aircraft = serializers.IntegerField()
    percentage = serializers.FloatField()

    class Meta:
        model = AircraftType
        fields = [
            "id",
            "production_name",
            "iata_code",
            "noise_level",
            "num_aircraft",
            "percentage",
            "images",
        ]


class AircraftSerializer(serializers.ModelSerializer):
    aircraft_type = AircraftTypeSummarySerializer()

    class Meta:
        model = Aircraft
        fields = [
            "id",
            "registration_number",
            "aircraft_type",
            "IATA_type",
            "model_code",
            "engine_count",
            "engine_type",
            "plane_class",
        ]


class AircraftSummarySerializer(serializers.ModelSerializer):
    aircraft_type_name = serializers.SerializerMethodField()

    def get_aircraft_type_name(self, obj):
        return AircraftTypeSummarySerializer(obj.aircraft_type).data["production_name"]

    class Meta:
        model = Aircraft
        fields = [
            "id",
            "registration_number",
            "aircraft_type_name",
            "IATA_type",
            "model_code",
        ]


class FlightPointSerializer(GeoModelSerializer):
    class Meta:
        model = FlightPoint
        fields = ["id", "timestamp", "position", "speed", "direction", "flight"]


class FlightSerializer(serializers.ModelSerializer):
    aircraft = AircraftSerializer()
    src = AirportSerializer()
    dst = AirportSerializer()

    class Meta:
        model = Flight
        fields = [
            "id",
            "flight_icao_number",
            "src",
            "dst",
            "airline_iata_number",
            "aircraft",
            "status",
        ]


class FlightSummarySerializer(serializers.ModelSerializer):
    aircraft = serializers.SerializerMethodField()

    def get_aircraft(self, obj):
        if obj.aircraft:
            return obj.aircraft.aircraft_type.production_name

    class Meta:
        model = Flight
        fields = [
            "id",
            "flight_icao_number",
            "airline_iata_number",
            "aircraft",
            "status",
        ]


class ScoreParameterSerializer(serializers.Serializer):
    latitude = serializers.FloatField(required=True)
    longitude = serializers.FloatField(required=True)


class CitySerializer(GeoModelSerializer):
    airports = AirportSerializer(many=True)
    position = serializers.SerializerMethodField()

    def get_position(self, obj):
        return {"latitude": obj.position[1], "longitude": obj.position[0]}

    class Meta:
        model = City
        fields = [
            "id",
            "position",
            "country",
            "score",
            "city_name",
            "airports",
            "images",
        ]


class CitySummarySerializer(GeoModelSerializer):
    class Meta:
        model = City
        fields = ["id", "country", "score", "city_name"]


class SearchResultSerializer(serializers.Serializer):
    pk = serializers.IntegerField()
    type = serializers.CharField()
    rank = serializers.FloatField()
