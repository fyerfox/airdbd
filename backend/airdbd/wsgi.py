"""
WSGI config for airdbd project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os, sys

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'airdbd.settings')
sys.path.append('/opt/python/current/app/backend')
sys.path.append('/opt/python/current/app/backend/airdbd')

application = get_wsgi_application()